$(document).ready(function(){
   $("#btnINSERT").click(function () {
      $("#divComments").html("");
      $("[name='char_Issue']").val("");
      $("[name='char_Description']").val("");
   });
   $("#CancelPost").click(function () {
      $("#divView").hide();
      $("#divList").show();
   });
   $("#PostIssue").click(function () {
      var hasErr = 0;
      $(".mandatory").each(function () {
         if ($(this).val() == "" || $(this).val() == "0") {
            $("[for='"+$(this).attr("id")+"']").show();
            hasErr++;
         } else {
            $("[for='"+$(this).attr("id")+"']").hide();
         }
      });
      if (hasErr == 0) $("#NewIssue").submit();
   });

   $("[name='btnFilter']").click(function () {
      var srchParam = "";
      if ($("[name='tID']").val() != "") srchParam += "&tID=" + $("[name='tID']").val();
      if ($("[name='tStatus']").val() != "") srchParam += "&tStatus=" + $("[name='tStatus']").val();
      if ($("[name='tCreated']").val() != "") srchParam += "&tCreated=" + $("[name='tCreated']").val();
      if ($("[name='tAssigned']").val() != "") srchParam += "&tAssigned=" + $("[name='tAssigned']").val();
      if ($("[name='tWorkingBy']").val() != "") srchParam += "&tWorkingBy=" + $("[name='tWorkingBy']").val();
      srchParam += setAddURL();
      gotoscrn($("#hProg").val(),srchParam);
   });



   /*$("#postIssue").click(function () {
      if ($("[name='char_Issue']").val() == "") {
         return false;
      }
      if ($("[name='char_Description']").val() == "") {
         return false;
      }
      $.ajax({
         url: "postComment.e2e.php",
         type: "POST",
         data: new FormData($('#NewIssue')[0]),
         success : function(responseTxt){
            if (responseTxt.trim() != "") {
               alert(responseTxt);
               return false;
            } else {
               gotoscrn ($("#hProg").val(), setAddURL());
            }
         },
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
         cache: false
      });
   });
   */
   $("#aSEND").click(function () {
      if ($("[name='char_Comments']").val() == "") {
         return false;
      }
      if ($("[name='char_CommentBy']").val() == "") {
         $("[name='char_CommentBy']").val("Err!!! No Assigned User");
      }
      $.ajax({
         url: "postComment.e2e.php",
         type: "POST",
         data: new FormData($('#formupload')[0]),
         success : function(responseTxt){
            $("[name='char_Comments']").val("");
            $("#message").html(responseTxt);
            selectMe($("[name='sint_issuesRefId']").val());
         },
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
         cache: false
      });
   });

   $(".chnStatus").each(function () {
      $(this).click(function () {
         if (confirm("Change Status Now?")) {
            $.post("postComment.e2e.php",
            {
               fn:"ChangeStatus",
               id:$(this).attr("id"),
               refid:$("[name='sint_issuesRefId']").val()
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                      if (e instanceof SyntaxError) {
                          alert(e.message);
                      }
                  }
               }
               else {
                  alert("Ooops Error : " + status);
               }
            });
         }
      });
   });

   $("#BackHome").click(function () {
      gotoscrn("scrnWelcome","");
   });
});

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   gotoscrn ($("#hProg").val(), setAddURL());

}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
   gotoscrn ($("#hProg").val(), setAddURL());

}
function afterDelete() {
   alert("Record Succesfully Deleted");
   gotoscrn ($("#hProg").val(), setAddURL());

}

function selectMe(IssuesRefId) {
   if (IssuesRefId > 0) {
      var url = "IssuesComments.e2e.php?id="+IssuesRefId;
      $("#divComments").html("");
      $("#divComments").load(url, function(responseTxt, statusTxt, xhr) {
         if(statusTxt == "error") {
            alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
         }
         if(statusTxt == "success") {
            return true;
         }
      });
   }

   /*
   $.ajax({
      url: url,
      success: function(result){
         $("#divComments").html(result);
      }
   });*/

}
