$(document).ready(function() {
   $("#btnCLOSE").click(function(){
      /*addParam = "&paramTitle=201 File";
      addParam += "&hTable=employees";
      addParam += "&hGridTblHdr=Last Name|First Name|Contact No|Birth Date|Civil Status";
      addParam += "&hGridTblFld=LastName|FirstName|ContactNo|BirthDate|CivilStatus";*/
      addParam = "&uplClose=Y";
      gotoscrn("scrnUsers",addParam);
   });
   $("#chkOveride").click(function(){
      if ($(this).is(":checked")) {
         $(this).val(1);
      } else {
         $(this).val(0);
      }
   });
   $("#minPDS").click(function(){
      $("#divbody").slideToggle(300);
   });
   $("#minPDSUpload").click(function(){
      $("#PDSUpload").slideToggle(300);
   });
   $("#closePDS").click(function(){
      $("#PDS").hide(300);
   });
   $("#printPDS").click(function(){
      printIFRAME("framePDS");
   });
   $("#btnUplCANCEL").click(function (){
      var addParam = "&paramTitle=EMPLOYEES FILE ATTACHMENT";
      gotoscrn("scrnEmpAttach",addParam);
   });

});

function printIFRAME(iframe_name) {
   window[iframe_name].focus();
   window[iframe_name].print();
}

function submitPage() {
   if ($("[name='txtRefId']").val() == "") {
         alert ("No Selected Employees!!!");
         return false;
   } else {
      $("#hSubmit").val("YES");
      document.eForm.submit();
   }
}