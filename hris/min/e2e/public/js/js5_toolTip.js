var dom = (document.getElementById) ? true : false;
var ns5 = (!document.all && dom || window.opera) ? true: false;
var ie5 = ((navigator.userAgent.indexOf("MSIE")>-1) && dom) ? true : false;
var ie4 = (document.all && !dom) ? true : false;
var nodyn = (!ns5 && !ie4 && !ie5 && !dom) ? true : false;
var origWidth, origHeight;
/*avoid error of passing event object in older browsers*/
if (nodyn) { event = "nope" }
/*// settings for tooltip
// Do you want tip to move when mouse moves over link?*/
var tipFollowMouse= true;
/*// Be sure to set tipWidth wide enough for widest image-*/
var bgColorTooltip = "#333";
var tipWidth = 400;
var offX = 5;	/* how far from mouse to show tip */
var offY = 10;
var tipFontFamily= "Calibri";
var tipFontSize = "8pt";
var tipFontWeight = 400;
/*// set default text color and background color for tooltip here
// individual tooltips can have their own (set in messages arrays)
// but don't have to*/
var tipFontColor= "#fff";
var tipBgColor = bgColorTooltip;
var tipBorderColor = "#000000";
var tipBorderWidth = 1;
var tipBorderStyle = "solid";
var tipPadding = 8;
/*erwin --- you can not include this, because you can make own msg depends on your passing parameters*/
/*// tooltip content goes here (image, description, optional bgColor, optional textcolor)*/
var messages = new Array();
/*// multi-dimensional arrays containing:
// image and text for tooltip
// optional: bgColor and color to be sent to tooltip*/
messages[0] = new Array("","Message one","#FFFFFF");
messages[1] = new Array("","Message two","#DDECFF");
messages[2] = new Array("","Message three","#000000","#FFFFCC");
/*////////////////////  END OF CUSTOMIZATION AREA  ///////////////////
// preload images that are to appear in tooltip
// from arrays above*/
if (document.images) {
	var theImgs = new Array();
	for (var i=0; i<messages.length; i++) {
  	theImgs[i] = new Image();
	theImgs[i].src = messages[i][0];
  }
}
/*// to layout image and text, 2-row table, image centered in top cell
// these go in var tip in doTooltip function
// startStr goes before image, midStr goes between image and TEXT*/
var startStr = "<table style='background-color:"+bgColorTooltip+";' width='"+tipWidth+"'><tr><td align='left' width='100%'>";
var midStr   = "</td></tr><tr><td valign='top'>";
var endStr   = "</td></tr></table>";
var tooltip, tipcss;

function initTip() {
	if (nodyn) return;
	tooltip = (ie4) ? document.all["tipDiv"]: (ie5||ns5)? document.getElementById("tipDiv"): null;
   tipcss = tooltip.style;
	if (ie4||ie5||ns5) {	// ns4 would lose all this on rewrites
		tipcss.width = tipWidth+"px";
		tipcss.fontFamily = tipFontFamily;
		tipcss.fontSize = tipFontSize;
		tipcss.color = tipFontColor;
		tipcss.backgroundColor = tipBgColor;
		tipcss.borderColor = tipBorderColor;
		tipcss.borderWidth = tipBorderWidth+"px";
		tipcss.padding = tipPadding+"px";
		tipcss.borderStyle = tipBorderStyle;
	}
	if (tooltip && tipFollowMouse) {
		document.onmousemove = trackMouse;
	}
}
window.onload = initTip;
var t1,t2;	/* for setTimeouts */
var tipOn = false;	/* check if over tooltip link */
function doTooltip(evt,num,FirstData) {
   if (t1) clearTimeout(t1);
	if (t2) clearTimeout(t2);
	tipOn = true;
	/* set colors if included in messages array*/
	if (messages[num][2])
      var curBgColor = messages[num][2];
	else
      curBgColor = tipBgColor;

	if (messages[num][3])
      var curFontColor = messages[num][3];
	else
      curFontColor = tipFontColor;

   if (ie4||ie5||ns5) {
      /*var tipstyle = "<span style='font-family:"+tipFontFamily+";font-weight:"+tipFontWeight+";font-size:"+tipFontSize+";color:"+curFontColor+";'>";
		var tip  = "<table style='background-color:" + bgColorTooltip + ";' width='" +tipWidth + "'>";
          tip += "   <tr>"
          tip += "      <td align='left' width='100%'>";
          tip += tipstyle + FirstData + "</span>" + midStr + tipstyle + "</span>";
          tip += "</td></tr></table>";
      tipcss.backgroundColor = curBgColor;
      */

	}
   tip = "<span class='azToolTip'>"+FirstData+"<span>";
	$("#tipDiv").html(tip);
	if (!tipFollowMouse) positionTip(evt);
	else t1 = setTimeout("tipcss.visibility='visible'",100);
}
var mouseX, mouseY;
function trackMouse(evt) {
	standardbody=(document.compatMode=="CSS1Compat")? document.documentElement : document.body /*create reference to common "body" across doctypes*/
	mouseX = (ns5)? evt.pageX: window.event.clientX + standardbody.scrollLeft;
	mouseY = (ns5)? evt.pageY: window.event.clientY + standardbody.scrollTop;
	if (tipOn) positionTip(evt);
}

function positionTip(evt) {
	if (!tipFollowMouse) {
		mouseX = (ns5)? evt.pageX: window.event.clientX + standardbody.scrollLeft;
		mouseY = (ns5)? evt.pageY: window.event.clientY + standardbody.scrollTop;
	}
	/*tooltip width and height*/
	var tpWd = (ie4||ie5)? tooltip.clientWidth: tooltip.offsetWidth;
	var tpHt = (ie4||ie5)? tooltip.clientHeight: tooltip.offsetHeight;
	/*document area in view (subtract scrollbar width for ns)*/
	var winWd = (ns5)? window.innerWidth-20+window.pageXOffset: standardbody.clientWidth+standardbody.scrollLeft;
	var winHt = (ns5)? window.innerHeight-20+window.pageYOffset: standardbody.clientHeight+standardbody.scrollTop;
	/*check mouse position against tip and window dimensions  */
	/*and position the tooltip*/
	if ((mouseX+offX+tpWd)>winWd)
		tipcss.left = mouseX-(tpWd+offX)+"px";
	else tipcss.left = mouseX+offX+"px";
	if ((mouseY+offY+tpHt)>winHt)
		tipcss.top = winHt-(tpHt+offY)+"px";
	else tipcss.top = mouseY+offY+"px";
	if (!tipFollowMouse) t1=setTimeout("tipcss.visibility='visible'",100);
}

function hideTip() {
	if (!tooltip) return;
	t2 = setTimeout("tipcss.visibility='hidden'",100);
	tipOn = false;
}
document.write("<div id='tipDiv' bgcolor='#CC0000' style='position:absolute; visibility:hidden; z-index:100;'></div>");
