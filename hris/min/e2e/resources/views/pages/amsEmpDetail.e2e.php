<?php
   session_start();
   include_once 'conn.e2e.php';
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   $EmpRefId = getvalue("EmpRefId");
   $AMSRow = FindFirst("ams_employees","WHERE EmployeesRefId = ".$EmpRefId,"*");
   if ($AMSRow) {
      $data = $AMSRow;
      echo '
         $("[name=\'char_LastName\']").val("'.mysqli_real_escape_string($conn,getRecord("employees",$data["EmployeesRefId"],"LastName")).'");
         $("[name=\'char_FirstName\']").val("'.mysqli_real_escape_string($conn,getRecord("employees",$data["EmployeesRefId"],"FirstName")).'");
         $("[name=\'char_MiddleName\']").val("'.mysqli_real_escape_string($conn,getRecord("employees",$data["EmployeesRefId"],"MiddleName")).'");
         $("[name=\'char_ExtName\']").val("'.mysqli_real_escape_string($conn,getRecord("employees",$data["EmployeesRefId"],"ExtName")).'");
         $("[name=\'date_BirthDate\']").val("'.mysqli_real_escape_string($conn,getRecord("employees",$data["EmployeesRefId"],"BirthDate")).'");
         $("[name=\'sint_AgencyId\']").val("'.mysqli_real_escape_string($conn,$data["AgencyId"]).'");
         $("[name=\'sint_PositionRefId\']").val("'.mysqli_real_escape_string($conn,$data["PositionRefId"]).'");
         $("[name=\'date_AssumptionDate\']").val("'.mysqli_real_escape_string($conn,$data["AssumptionDate"]).'");
         $("[name=\'date_ResignedDate\']").val("'.mysqli_real_escape_string($conn,$data["ResignedDate"]).'");
         $("[name=\'date_RehiredDate\']").val("'.mysqli_real_escape_string($conn,$data["RehiredDate"]).'");
         $("[name=\'date_HiredDate\']").val("'.mysqli_real_escape_string($conn,$data["HiredDate"]).'");
         $("[name=\'sint_PositionItemRefId\']").val("'.mysqli_real_escape_string($conn,$data["PositionItemRefId"]).'");
         $("[name=\'sint_OfficeRefId\']").val("'.mysqli_real_escape_string($conn,$data["OfficeRefId"]).'");
         $("[name=\'sint_DepartmentRefId\']").val("'.mysqli_real_escape_string($conn,$data["DepartmentRefId"]).'");
         $("[name=\'sint_DivisionRefId\']").val("'.mysqli_real_escape_string($conn,$data["DivisionRefId"]).'");
         $("[name=\'sint_EmpStatusRefId\']").val("'.mysqli_real_escape_string($conn,$data["EmpStatusRefId"]).'");
         $("[name=\'sint_SalaryGradeRefId\']").val("'.mysqli_real_escape_string($conn,$data["SalaryGradeRefId"]).'");
         $("[name=\'sint_StepIncrementRefId\']").val("'.mysqli_real_escape_string($conn,$data["StepIncrementRefId"]).'");
         $("[name=\'deci_SalaryAmount\']").val("'.mysqli_real_escape_string($conn,$data["SalaryAmount"]).'");
         $("[name=\'char_PayPeriod\']").val("'.mysqli_real_escape_string($conn,$data["PayPeriod"]).'");
         $("[name=\'sint_WorkScheduleRefId\']").val("'.mysqli_real_escape_string($conn,$data["WorkScheduleRefId"]).'");
         $("[name=\'char_BiometricsID\']").val("'.mysqli_real_escape_string($conn,$data["BiometricsID"]).'");
      ';
   } else {
      $Employees = FindFirst("employees","WHERE RefId = $EmpRefId","*");
      $where = "WHERE CompanyRefId = ".getvalue("hCompanyID")." AND BranchRefId = ".getvalue("hBranchID")." AND EmployeesRefId = $EmpRefId";
      if ($Employees) {
         $EmpInfo = FindFirst("empinformation",$where,"*");
         if ($EmpInfo) {
            $data = array_merge($Employees,$EmpInfo);
            echo '
               $("[name=\'char_LastName\']").val("'.mysqli_real_escape_string($conn,$data["LastName"]).'");
               $("[name=\'char_FirstName\']").val("'.mysqli_real_escape_string($conn,$data["FirstName"]).'");
               $("[name=\'char_MiddleName\']").val("'.mysqli_real_escape_string($conn,$data["MiddleName"]).'");
               $("[name=\'char_ExtName\']").val("'.mysqli_real_escape_string($conn,$data["ExtName"]).'");
               $("[name=\'date_BirthDate\']").val("'.mysqli_real_escape_string($conn,$data["BirthDate"]).'");
               $("[name=\'sint_AgencyId\']").val("'.mysqli_real_escape_string($conn,$data["AgencyId"]).'");
               $("[name=\'sint_PositionRefId\']").val("'.mysqli_real_escape_string($conn,$data["PositionRefId"]).'");
               $("[name=\'date_AssumptionDate\']").val("'.mysqli_real_escape_string($conn,$data["AssumptionDate"]).'");
               $("[name=\'date_ResignedDate\']").val("'.mysqli_real_escape_string($conn,$data["ResignedDate"]).'");
               $("[name=\'date_RehiredDate\']").val("'.mysqli_real_escape_string($conn,$data["RehiredDate"]).'");
               $("[name=\'date_HiredDate\']").val("'.mysqli_real_escape_string($conn,$data["HiredDate"]).'");
               $("[name=\'sint_PositionItemRefId\']").val("'.mysqli_real_escape_string($conn,$data["PositionItemRefId"]).'");
               $("[name=\'sint_OfficeRefId\']").val("'.mysqli_real_escape_string($conn,$data["OfficeRefId"]).'");
               $("[name=\'sint_DepartmentRefId\']").val("'.mysqli_real_escape_string($conn,$data["DepartmentRefId"]).'");
               $("[name=\'sint_DivisionRefId\']").val("'.mysqli_real_escape_string($conn,$data["DivisionRefId"]).'");
               $("[name=\'sint_EmpStatusRefId\']").val("'.mysqli_real_escape_string($conn,$data["EmpStatusRefId"]).'");
               $("[name=\'sint_SalaryGradeRefId\']").val("'.mysqli_real_escape_string($conn,$data["SalaryGradeRefId"]).'");
               $("[name=\'sint_StepIncrementRefId\']").val("'.mysqli_real_escape_string($conn,$data["StepIncrementRefId"]).'");
               $("[name=\'deci_SalaryAmount\']").val("'.mysqli_real_escape_string($conn,$data["SalaryAmount"]).'");
               $("[name=\'char_PayPeriod\']").val("'.mysqli_real_escape_string($conn,$data["PayPeriod"]).'");
               $("[name=\'sint_WorkScheduleRefId\']").val("'.mysqli_real_escape_string($conn,$data["WorkScheduleRefId"]).'");
               $("[name=\'char_BiometricsID\']").val("'.mysqli_real_escape_string($conn,$data["BiometricsID"]).'");

            ';
            //$("[name=\'sint_PayrateRefId\']").val("'.mysqli_real_escape_string($conn,$data["PayrateRefId"]).'");
         }
      } else {
         echo "No Record";
      }
   }
























   
?>