<?php
   session_start();
   require_once "constant.e2e.php";
   require_once "conn.e2e.php";
   require_once pathClass."0620functions.e2e.php";
   $_SESSION["SelectedEMP"] = getvalue("selected");
   if (getvalue("hucode") == "COMPEMP") {
      $EmpRefId = $_SESSION["EmployeesRefId"];
   } else {
      $EmpRefId = getvalue("EmpRefId");
   }
   $rs = FindFirst("employees","where RefId = $EmpRefId","*");
   $row = $rs;   
?>
<script type="text/javascript" src="<?php echo jsCtrl("ctrl_201File"); ?>"></script>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanPerInfo"><i class="fa fa-caret-down" aria-hidden="true" id="iPerInfo"></i></span>&nbsp;
      I. PERSONAL INFORMATION
   </div>
   <div class="panel-mid" id="panelPerInfo">
      <div class="row">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >LAST NAME</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["LastName"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >FIRST NAME</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["FirstName"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >MIDDLE NAME</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["MiddleName"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >EXT. NAME</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["ExtName"];?></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >DATE OF BIRTH</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["BirthDate"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >PLACE OF BIRTH</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["BirthPlace"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="">
                     <label class="control-label" >SEX</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["Sex"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div>
                     <div class="">
                        <label class="control-label" >CITIZENSHIP</label><br>
                        <div class="fieldValue--">&nbsp;
                           <?php dispCitizen($row["isFilipino"]); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >CIVIL STATUS</label><br>
                     <div class="fieldValue--">&nbsp;<?php dispCivilStatus($row["CivilStatus"]); ?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="form-group">
                           <label class="control-label" >HEIGHT (m)</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["Height"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-6">
                        <div class="form-group">
                           <label class="control-label" >WEIGHT (kg)</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["Weight"];?></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">BLOOD TYPE</label><br>

                     <div class="fieldValue--">&nbsp;<?php

                        echo getRecord("BloodType",$row["BloodTypeRefId"],"Name");
                        ?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >AGENCY EMPLOYEE NO.</label><br>
                     <div class="fieldValue--">&nbsp;<?php echo $row["AgencyId"]; ?></div>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >CONTACT NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["ContactNo"]; ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >GOVT. ISSUED ID</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["GovtIssuedID"]; ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                        <label class="control-label" >GOVT. ISSUED ID No.</label><br>
                        <div class="fieldValue--">&nbsp;<?php echo $row["GovtIssuedIDNo"]; ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                        <label class="control-label" >GOVT. ISSUED ID Place</label><br>
                        <div class="fieldValue--">&nbsp;<?php echo $row["GovtIssuedIDPlace"]; ?></div>
                        </div>
                     </div>
                  </div>

            <div class="mypanel">
               <div class="panel-top">ADDRESS</div>
               <div class="panel-mid">
                  <h4 style="color:blue;font-weight:600;">Residential Address</h4>
                  <?php bar(); ?>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">HOUSE NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["ResiHouseNo"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">STREET</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["ResiStreet"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">BRGY.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["ResiBrgy"];?></div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >COUNTRY</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo getRecord("Country",$row["ResiCountryRefId"],"Name") ;?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PROVINCE</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo getRecord("Province",$row["ResiAddProvinceRefId"],"Name"); ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >CITY</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo getRecord("City",$row["ResiAddCityRefId"],"Name"); ?></div>
                        </div>
                     </div>

                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >ZIPCODE</label><br>
                           <div class="fieldValue--">&nbsp;</div>
                        </div>
                     </div>
                  </div>

                  <h4 style="color:blue;font-weight:600;">Permanent Address</h4>
                  <?php bar(); ?>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">HOUSE NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["PermanentHouseNo"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">STREET</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["PermanentStreet"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">BRGY.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["PermanentBrgy"];?></div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >COUNTRY</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo getRecord("Country",$row["PermanentCountryRefId"],"Name"); ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PROVINCE</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo getRecord("Province",$row["PermanentAddProvinceRefId"],"Name"); ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >CITY</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo getRecord("City",$row["PermanentAddCityRefId"],"Name"); ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >ZIPCODE</label><br>
                           <div class="fieldValue--">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php spacer(5); ?>
            <div class="mypanel">
               <div class="panel-top">CONTACT's</div>
               <div class="panel-mid">
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >TELEPHONE NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["TelNo"]; ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >MOBILE NO</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["MobileNo"]; ?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >E-MAIL ADDRESS (if any)</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["EmailAdd"]; ?></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php spacer(5); ?>
            <div class="mypanel">
               <div class="panel-top">ID's</div>
               <div class="panel-mid">
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >GSIS ID NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["GSIS"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PAGIBIG ID NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["PAGIBIG"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PHILHEALTH NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["PHIC"];?></div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >TIN NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["TIN"];?></div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >SSS NO.</label><br>
                           <div class="fieldValue--">&nbsp;<?php echo $row["SSS"];?></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   $rs = FindFirst("employeesfamily","where EmployeesRefId = $EmpRefId","*");
   $row = $rs;   
?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanFamBG"><i class="fa fa-caret-down" aria-hidden="true" id="iFamBG"></i></span>&nbsp;
      II. FAMILY BACKGROUND
   </div>
   <div class="panel-mid" id="panelFamBG">
         <div class="panel-top">
            SPOUSE's INFORMATION
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputSLName">LAST NAME:</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["SpouseLastName"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputSFName">FIRST NAME:</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["SpouseFirstName"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputSMName">MIDDLE NAME:</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["SpouseMiddleName"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >EXT. NAME</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["SpouseExtName"];?></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >DATE OF BIRTH :</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["SpouseBirthDate"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >MOBILE NO.:</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["SpouseMobileNo"];?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputOccupation">OCCUPATION:</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo getRecord("Occupations",$row["OccupationsRefId"],"Name"); ?></div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >EMPLOYER's NAME</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["EmployersName"];?></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-6">
                  <div class="form-group">
                     <label class="control-label" >BUSINESS ADDRESS</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $row["BusinessAddress"];?></div>
                  </div>
               </div>

            </div>
         </div>
         <?php spacer(5); ?>
         <div class="panel-top">
            CHILDREN's INFORMATION
         </div>
         <div class="panel-mid" id="contentChildrens">
            <?php
               $rs = SelectEach("employeeschild","where EmployeesRefId = $EmpRefId ORDER BY BirthDate DESC");
               if ($rs) {
                  while($rowChild = mysqli_fetch_array($rs))
                  {
            ?>
            <div class="row margin-top">
               <div class="col-xs-6">
                  <div class="form-group">
                     <label class="control-label" >FULL NAME</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $rowChild["FullName"];?></div>
                  </div>
               </div>
               <div class="col-xs-2">
                  <div class="form-group">
                     <label class="control-label" >SEX</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $rowChild["Gender"];?></div>
                  </div>
               </div>
               <div class="col-xs-2">
                  <div class="form-group">
                     <label class="control-label" for="inputSFName">DATE OF BIRTH</label>
                     <br><div class="fieldValue--">&nbsp;<?php echo $rowChild["BirthDate"];?></div>
                  </div>
               </div>
            </div>
            <?php
                  }
               } else {
                  echo '<div style="color:red;">NO RECORD FOUND</div>';
               }
            ?>
         </div>

         <?php
            $rs = FindFirst("employeesfamily","where EmployeesRefId = $EmpRefId","*");
            $row = $rs;
         ?>
            <?php spacer(5); ?>
            <div class="panel-top">
               FATHER's INFORMATION
            </div>
            <div class="panel-mid">
               <div class="row">
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputFLName">LAST NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["FatherLastName"];?></div>
                     </div>
                  </div>

                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputFFName">FIRST NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["FatherFirstName"];?></div>
                     </div>
                  </div>
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputFMName">MIDDLE NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["FatherMiddleName"];?></div>
                     </div>
                  </div>
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputFXName">EXT NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["FatherExtName"];?></div>
                     </div>
                  </div>
               </div>
            </div>
            <?php spacer(5); ?>
            <div class="panel-top">
               MOTHER's INFORMATION (Maiden Name)
            </div>
            <div class="panel-mid">
               <div class="row">
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputMLName">LAST NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["MotherLastName"];?></div>
                     </div>
                  </div>
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputMFName">FIRST NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["MotherFirstName"];?></div>
                     </div>
                  </div>
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputMMName">MIDDLE NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["MotherMiddleName"];?></div>
                     </div>
                  </div>
                  <div class="col-xs-3">
                     <div class="form-group">
                        <label class="control-label" for="inputMXName">EXT NAME:</label>
                        <br><div class="fieldValue--">&nbsp;<?php echo $row["MotherExtName"];?></div>
                     </div>
                  </div>
               </div>
            </div>


   </div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanEducBG"><i class="fa fa-caret-down" aria-hidden="true" id="iEducBG"></i></span>&nbsp;
      III. EDUCATIONAL BACKGROUND
   </div>
   <div class="panel-mid" id="panelEducBG">
      <div class="panel-top">
         <div class="row txt-center">
            <div class="col-xs-1">
               LEVEL
            </div>
            <div class="col-xs-2">
               NAME OF SCHOOL (Write in full)
            </div>
            <div class="col-xs-2">
               DEGREE COURSE (Write in full)
            </div>
            <div class="col-xs-1">
               YEAR GRADUATED (if graduated)
            </div>
            <div class="col-xs-2">
               HIGHEST GRADE/LEVEL/UNITS EARNED(if not graduated)
            </div>
            <div class="col-xs-2">
               <div class="row">
                  INCLUSIVE DATES OF ATTENDANCE
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     FROM
                  </div>
                  <div class="col-xs-6">
                     TO
                  </div>
               </div>
            </div>
            <div class="col-xs-2">
               SCHOLARSHIP/<br>ACADEMIC HONORS RECEIVED
            </div>
         </div>
         <hr>
      </div>
      <?php spacer(5); ?>
      <div class="panel-top">ELEMENTARY</div>
      <div class="panel-mid">
         <?php
            $rs = SelectEach("employeeseduc","where EmployeesRefId = $EmpRefId and LevelType = 1");
            if ($rs) {
               while($row = mysqli_fetch_array($rs))
               {
         ?>
                  <div class="row margin-top" id="Elementary">
                     <div class="col-xs-1">
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Schools",$row["SchoolsRefId"],"Name");?>
                     </div>
                     <div class="col-xs-2">
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["YearGraduated"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["HighestGrade"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateFrom"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateTo"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["Honors"];?>
                     </div>
                  </div>

         <?php
               }
            } else {
               echo '<div style="color:red;">NO RECORD FOUND</div>';
            }
         ?>
      </div>
      <?php spacer(5); ?>
      <div class="panel-top">SECONDARY</div>
      <div class="panel-mid">
         <?php
            $rs = SelectEach("employeeseduc","where EmployeesRefId = $EmpRefId and LevelType = 2");
            if ($rs) {
               while($row = mysqli_fetch_array($rs))
               {
         ?>

                  <div class="row margin-top" id="Secondary">
                     <div class="col-xs-1">
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Schools",$row["SchoolsRefId"],"Name");?>
                     </div>
                     <div class="col-xs-2">
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["YearGraduated"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["HighestGrade"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateFrom"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateTo"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["Honors"];?>
                     </div>
                  </div>

         <?php
               }
            } else {
               echo '<div style="color:red;">NO RECORD FOUND</div>';
            }
         ?>
      </div>
      <?php spacer(5); ?>
      <div class="panel-top">VOCATIONAL</div>
      <div class="panel-mid">
         <?php
            $rs = SelectEach("employeeseduc","where EmployeesRefId = $EmpRefId and LevelType = 3");
            if ($rs) {
               while($row = mysqli_fetch_array($rs))
               {
         ?>

                  <div class="row margin-top" id="Vocational">
                     <div class="col-xs-1">
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Schools",$row["SchoolsRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Course",$row["CourseRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["YearGraduated"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["HighestGrade"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateFrom"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateTo"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["Honors"];?>
                     </div>
                  </div>

         <?php
               }
            } else {
               echo '<div style="color:red;">NO RECORD FOUND</div>';
            }
         ?>
      </div>
      <?php spacer(5); ?>
      <div class="panel-top">COLLEGE</div>
      <div class="panel-mid">
         <?php
            $rs = SelectEach("employeeseduc","where EmployeesRefId = $EmpRefId and LevelType = 4");
            if ($rs) {
               while($row = mysqli_fetch_array($rs))
               {
         ?>


                  <div class="row margin-top" id="College">
                     <div class="col-xs-1">
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Schools",$row["SchoolsRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Course",$row["CourseRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["YearGraduated"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["HighestGrade"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateFrom"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateTo"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["Honors"];?>
                     </div>
                  </div>

         <?php
               }
            } else {
               echo '<div style="color:red;">NO RECORD FOUND</div>';
            }
         ?>
      </div>
      <?php spacer(5); ?>
      <div class="panel-top">GRADUATED STUDIES</div>
      <div class="panel-mid">
         <?php
            $rs = SelectEach("employeeseduc","where EmployeesRefId = $EmpRefId and LevelType = 5");
            if ($rs) {
               while($row = mysqli_fetch_array($rs))
               {
         ?>


                  <div class="row margin-top" id="GradStudies">
                     <div class="col-xs-1">
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Schools",$row["SchoolsRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo getRecord("Course",$row["CourseRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["YearGraduated"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["HighestGrade"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateFrom"];?>
                     </div>
                     <div class="col-xs-1 fieldValue--">
                        &nbsp;<?php echo $row["DateTo"];?>
                     </div>
                     <div class="col-xs-2 fieldValue--">
                        &nbsp;<?php echo $row["Honors"];?>
                     </div>
                  </div>
         <?php
               }
            } else {
               echo '<div style="color:red;">NO RECORD FOUND</div>';
            }
         ?>
      </div>
   </div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanEligib"><i class="fa fa-caret-down" aria-hidden="true" id="iEligib"></i></span>&nbsp;
      IV. ELIGIBILITY
   </div>
   <div class="panel-mid" id="panelEligib">
      <div class="panel-top">
         <div class="row txt-center">
            <div class="col-xs-4">CAREER SERVICE/RA 1080 (BOARD/BAR)<br>UNDER SPECIAL LAWS/CES/CSEE</div>
            <div class="col-xs-2">RATING</div>
            <div class="col-xs-2">DATE OF EXAMINATION/<br>CONFERMENT</div>
            <div class="col-xs-4">PLACE OF EXAMINATION /<br>CONFERMENT</div>
         </div>
         <hr>
      </div>
      <?php
         $rs = SelectEach("employeeselegibility","where EmployeesRefId = $EmpRefId order by ExamDate DESC");
         if ($rs) {
            while($row = mysqli_fetch_array($rs))
            {
      ?>
               <div class="panel-mid">
                  <div class="row margin-top padd5">
                     <div class="col-xs-4 txt-center fieldValue--">
                        &nbsp;<?php echo getRecord("CareerService",$row["CareerServiceRefId"],"Name"); ?>
                     </div>
                     <div class="col-xs-2 txt-center fieldValue--">
                        &nbsp;<?php echo $row["Rating"];?>
                     </div>
                     <div class="col-xs-2 txt-center fieldValue--">
                        &nbsp;<?php echo $row["ExamDate"];?>
                     </div>
                     <div class="col-xs-4 txt-center fieldValue--">
                        &nbsp;<?php echo $row["ExamPlace"];?>
                     </div>
                  </div>
                  <div class="row margin-top padd5">
                     <div class="col-xs-3 txt-center fieldValue--">
                        &nbsp;<?php echo $row["LicenseNo"];?>
                     </div>
                     <div class="col-xs-3 txt-center fieldValue--">
                        &nbsp;<?php echo $row["LicenseReleasedDate"];?>
                     </div>
                  </div>
               </div>
      <?php
            }
         } else {
            echo '<div style="color:red;">NO RECORD FOUND</div>';
         }
      ?>
   </div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanWExp"><i class="fa fa-caret-down" aria-hidden="true" id="iWExp"></i></span>&nbsp;
      V. WORK EXPERIENCE
   </div>
   <div class="panel-mid" id="panelWExp">

      <div class="panel-top">
         <div class="row">
            <div class="col-xs-3 txt-center">
               <div class="row">
                  <div class="col-xs-12 txt-center">
                     INCLUSIVE DATES<br>(mm/dd/yyyy)
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 txt-center">
                     FROM
                  </div>
                  <div class="col-xs-6 txt-center">
                     TO
                  </div>
               </div>
            </div>
            <div class="col-xs-3 txt-center">
               POSITION TITLE<br>(Write in Full)
            </div>
            <div class="col-xs-3 txt-center">
               DEPT/AGENCY/OFFICE<br>(Write in Full)
            </div>
            <div class="col-xs-3 txt-center">
               MONTHLY<br>SALARY
            </div>
         </div>
         <hr>
      </div>
      <?php
         $rs = SelectEach("employeesworkexperience","where EmployeesRefId = $EmpRefId order by WorkStartDate desc");
         if ($rs) {
            while($row = mysqli_fetch_array($rs))
            {
      ?>
            <div>
               <div class="row margin-top padd5">
                  <div class="col-xs-3 txt-center">
                     <div class="row">
                        <div class="col-xs-6 txt-center fieldValue--">
                           &nbsp;<?php echo $row["WorkStartDate"];?>
                        </div>
                        <div class="col-xs-6 txt-center fieldValue--">
                           &nbsp;<?php echo $row["WorkEndDate"];?>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo getRecord("Position",$row["PositionRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo getRecord("Agency",$row["AgencyRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo $row["SalaryAmount"];?>
                  </div>
               </div>

               <div class="row margin-top padd5">
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo getRecord("SalaryGrade",$row["SalaryGradeRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo getRecord("ApptStatus",$row["ApptStatusRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-2 fieldValue--">
                     &nbsp;<?php if ($row["isGovtService"]) echo "YES"; else echo "NO"; ?>
                  </div>
                  <div class="col-xs-2 fieldValue--">
                     &nbsp;<?php echo $row["PayGroup"];?>
                  </div>
                  <div class="col-xs-2 fieldValue--">
                     &nbsp;<?php echo getRecord("PayRate",$row["PayrateRefId"],"Name"); ?>
                  </div>
               </div>

               <div class="row margin-top padd5">
                  <div class="col-xs-2 fieldValue--">
                     &nbsp;<?php echo $row["LWOP"];?>
                  </div>
                  <div class="col-xs-2 fieldValue--">
                     &nbsp;<?php echo $row["ExtDate"];?>
                  </div>
                  <div class="col-xs-2 fieldValue--">
                     &nbsp;<?php echo $row["SeparatedDate"];?>
                  </div>
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo $row["Reason"];?>
                  </div>
                  <div class="col-xs-3 fieldValue--">
                     &nbsp;<?php echo $row["Remarks"];?>
                  </div>
               </div>
            </div>
      <?php
           }
         } else {
            echo '<div style="color:red;">NO RECORD FOUND</div>';
         }
      ?>
   </div>
</div>
<?php spacer(5);?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanVolWork"><i class="fa fa-caret-down" aria-hidden="true" id="iVolWork"></i></span>&nbsp;
      VI. VOLUNTARY WORK
   </div>
   <div class="panel-mid" id="panelVolWork">
      <div class="panel-top">
         <div class="row txt-center">
            <div class="col-xs-3">
               NAME AND ADDRESS OF ORGANIZATION<BR>
               (write in full)
            </div>
            <div class="col-xs-3">
               <div class="row">
                  INCLUSIVE DATES<br>
                  (mm/dd/yyyy)
               </div>
               <div class="row">
                  <div class="col-xs-6">FROM</div>
                  <div class="col-xs-6">TO</div>
               </div>
            </div>
            <div class="col-xs-2">
               NUMBER OF HOURS
            </div>
            <div class="col-xs-4">
               POSITION NATURE OF WORK
            </div>
         </div>
         <hr>
      </div>
      <?php
         $rs = SelectEach("employeesvoluntary","where EmployeesRefId = $EmpRefId order by StartDate desc");
         if ($rs) {
            while($row = mysqli_fetch_array($rs))
            {
         ?>
         <div class="row margin-top padd5">
            <div class="col-xs-3 txt-center fieldValue--">
               &nbsp;<?php echo getRecord("Organization",$row["OrganizationRefId"],"Name"); ?>
            </div>
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-6 fieldValue--">
                     &nbsp;<?php echo $row["StartDate"];?>
                  </div>
                  <div class="col-xs-6 fieldValue--">
                     &nbsp;<?php echo $row["EndDate"];?>
                  </div>
               </div>
            </div>
            <div class="col-xs-2 fieldValue--">
               &nbsp;<?php echo $row["NumofHrs"];?>
            </div>
            <div class="col-xs-4 fieldValue--">
               &nbsp;<?php echo $row["WorksNature"];?>
            </div>
         </div>
      <?php
           }
         } else {
            echo '<div style="color:red;">NO RECORD FOUND</div>';
         }
      ?>
   </div>
</div>
<?php spacer(5);?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanTrainProg"><i class="fa fa-caret-down" aria-hidden="true" id="iTrainProg"></i></span>&nbsp;
      VII. TRAINING PROGRAMS
   </div>
   <div class="panel-mid" id="panelTrainProg">
      <div class="panel-top">
         <div class="row">
            <div class="col-xs-3 txt-center">
               TITLE OF SEMINAR/CONFERENCE
               /WORKSHOP/SHORTCOURSES
               <br>(Write in full)
            </div>
            <div class="col-xs-2 txt-center">
               <div class="row">
                  INCLUSIVE DATES
               </div>
               <div class="row">
                  <div class="col-xs-6 txt-center margin-top">
                     FROM
                  </div>
                  <div class="col-xs-6 txt-center margin-top">
                     TO
                  </div>
               </div>
            </div>
            <div class="col-xs-1 txt-center">
               NUMBERS OF HOURS
            </div>
            <div class="col-xs-2 txt-center">
               CONDUCTED SPONSORED BY
               (Write in full)
            </div>
            <div class="col-xs-2 txt-center" style="margin-top:15px;">
               PLACE
            </div>
            <div class="col-xs-2 txt-center" style="margin-top:10px;">
               SEMINAR TYPE
            </div>
         </div>
         <hr>
      </div>
      <?php
         $rs = SelectEach("employeestraining","where EmployeesRefId = $EmpRefId order by StartDate");
         if ($rs) {
            while($row = mysqli_fetch_array($rs))
            {
      ?>
               <div class="row margin-top padd5">
                  <div class="col-xs-3 txt-center fieldValue--">
                     &nbsp;<?php echo getRecord("Seminars",$row["SeminarsRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-1 txt-center fieldValue--">
                     &nbsp;<?php echo $row["StartDate"];?>
                  </div>
                  <div class="col-xs-1 txt-center fieldValue--">
                     &nbsp;<?php echo $row["EndDate"];?>
                  </div>
                  <div class="col-xs-1 txt-center fieldValue--">
                     &nbsp;<?php echo $row["NumofHrs"];?>
                  </div>
                  <div class="col-xs-2 txt-center fieldValue--">
                     &nbsp;<?php echo getRecord("Sponsor",$row["SponsorRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-2 txt-center fieldValue--">
                     &nbsp;<?php echo getRecord("SeminarPlace",$row["SeminarPlaceRefId"],"Name"); ?>
                  </div>
                  <div class="col-xs-2 txt-center fieldValue--">
                     &nbsp;<?php echo getRecord("SeminarType",$row["SeminarTypeRefId"],"Name"); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1">Description</div>
                  <div class="col-xs-6 fieldValue--">
                     &nbsp;<?php echo $row["Description"];?>
                  </div>
               </div>
               <hr>
      <?php
           }
         } else {
            echo '<div style="color:red;">NO RECORD FOUND</div>';
         }
      ?>
   </div>
</div>
<?php spacer(5);?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanOtherInfo"><i class="fa fa-caret-down" aria-hidden="true" id="iOtherInfo"></i></span>&nbsp;
      VIII. OTHER INFORMATION
   </div>
   <div class="panel-mid" id="panelOtherInfo">
      <div class="panel-top">
         <div class="row">
            <div class="col-xs-4 txt-center">
               SPECIAL SKILLS/HOBBIES
            </div>
            <div class="col-xs-4 txt-center">
               NON-ACADEMIC DISTINCTION/RECOGNITION<BR>
               (Write in full)
            </div>
            <div class="col-xs-4 txt-center">
               MEMBERSHIP IN ASSOCIATION/ORGANIZATION<br>
               (Write in full)
            </div>
         </div>
         <hr>
      </div>
      <?php
         $rs = SelectEach("employeesotherinfo","where EmployeesRefId = $EmpRefId");
         if ($rs) {
            while($row = mysqli_fetch_array($rs))
            {
      ?>
               <div class="row margin-top padd5">
                  <div class="col-xs-4 txt-center fieldValue--">
                     &nbsp;<?php echo $row["Skills"];?>
                  </div>
                  <div class="col-xs-4 txt-center fieldValue--">
                     &nbsp;<?php echo $row["Recognition"];?>
                  </div>
                  <div class="col-xs-4 txt-center fieldValue--">
                     &nbsp;<?php echo $row["Affiliates"];?>
                  </div>
               </div>
               <hr>
      <?php
           }
         } else {
            echo '<div style="color:red;">NO RECORD FOUND</div>';
         }
      ?>
   </div>
</div>
<?php spacer(5);?>
<div class="mypanel">
   <?php
      $rs = FindFirst("employeespdsq","where EmployeesRefId = $EmpRefId","*");
      $row = $rs;
   ?>
   <div class="panel-top">
      <span id="spanPDSQ"><i class="fa fa-caret-down" aria-hidden="true" id="iPDSQ"></i></span>&nbsp;
      IX. PDS QUESTION
   </div>
   <div class="panel-mid" id="panelPDSQ">
   <div class="row">
      <?php spacer(5); ?>
      <div class="col-xs-6">
         <div class="panel-top">
            34. Are you related by consanguinity or affinity to the appointing or recommending authority, or to the chief of bureau or office or to the person who has immediate supervision over you in the Office, Bureau or Department where you will be apppointed,
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. within the third degree?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q1a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q1aexp"];?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. within the fourth degree (for Local Government Unit - Career Employees)?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q1b"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q1bexp"]; ?>
               </div>
            </div>
         </div>

         <div class="panel-top margin-top">
            35.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been found guilty of any administrative offense?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q2a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q2aexp"]; ?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. Have you been criminally charged before any court?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q2b"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q2bexp"];?>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            36.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been convicted of any crime or violation of any law, decree, ordinance or regulation by any court or tribunal?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q3a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q3aexp"];?>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-6">
         <div class="panel-top">
            37.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been separated from the service in any of the following modes: resignation, retirement, dropped from the rolls, dismissal, termination, end of term, finished contract or phased out (abolition) in the public or private sector?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q4a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q4aexp"];?>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            38.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been a candidate in a national or local election held within the last year (except Barangay election)?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q5a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q5aexp"];?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. Have you resigned from the government service during the three (3)-month period before the last election to promote/actively campaign for a national or local candidate?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q5b"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q5bexp"];?>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            39.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you acquired the status of an immigrant or permanent resident of another country?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q6a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q6aexp"];?>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            40. Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277); and (c) Solo Parents Welfare Act of 2000 (RA 8972), please answer the following items:
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Are you a member of any indigenous group?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q7a"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details<br>&nbsp;<?php echo $row["Q7aexp"]; ?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. Are you a person with disability?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q7b"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, please specify ID no.<br>&nbsp;<?php echo $row["Q7bexp"]; ?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  c. Are you a solo parent?<br>
                  <div class="fieldValue--">&nbsp;<?php dispYN($row["Q7c"]); ?></div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, please specify ID no.<br>&nbsp;<?php echo $row["Q7cexp"]; ?>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
</div>
<?php spacer(5);?>
<div class="mypanel">
   <div class="panel-top">
      <span id="spanRef"><i class="fa fa-caret-down" aria-hidden="true" id="iRef"></i></span>&nbsp;
      X. REFERENCE
   </div>
   <div class="panel-mid" id="panelRef">
      <div class="panel-top">
         <div class="row">
            <div class="col-xs-4 txt-center">NAME</div>
            <div class="col-xs-4 txt-center">ADDRESS</div>
            <div class="col-xs-4 txt-center">TELEPHONE NO.</div>
         </div>
      </div>
   <?php
      $rs = SelectEach("employeesreference","where EmployeesRefId = $EmpRefId");
      if ($rs) {
         while($row = mysqli_fetch_array($rs))
         {
   ?>

         <div class="row margin-top padd5">
            <div class="col-xs-4 txt-center fieldValue--">
               &nbsp;<?php echo $row["Name"];?>
            </div>
            <div class="col-xs-4 txt-center fieldValue--">
               &nbsp;<?php echo $row["Address"];?>
            </div>
            <div class="col-xs-4 txt-center fieldValue--">
               &nbsp;<?php echo $row["ContactNo"];?>
            </div>
         </div>

   <?php
        }
      } else {
         echo '<div style="color:red;">NO RECORD FOUND</div>';
      }
   ?>
   </div>
</div>
<script language="javascript" src="<?php echo jsCtrl("ctrl_MasterFile"); ?>"></script>

