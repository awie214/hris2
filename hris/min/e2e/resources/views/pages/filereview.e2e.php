<style>
   .BackToTop {
      color : #fff;
      font-size : 9px;
      font-weight : 400;
   }
   .BackToTop:hover {
      color : #fff;
      text-decoration:underline;
   }
   #Top > a {
      color : blue;
      font-size : 9px;
      font-weight : 400;
   }
</style>

<div id="Top">
   <a href="#PersonalSection">PERSONAL INFORMATION</a>&nbsp;|&nbsp;
   <a href="#ChildrenSection">CHILDREN INFORMATION</a>&nbsp;|&nbsp;
   <a href="#FamilySection">FAMILY BACKGROUND</a>&nbsp;|&nbsp;
   <a href="#EducSection">EDUCATIONAL BACKROUND</a>&nbsp;|&nbsp;
   <a href="#EligibilitySection">ELIGIBILITY</a>&nbsp;|&nbsp;
   <a href="#WorkExpSection">WORK EXPERIENCE</a>&nbsp;|&nbsp;
   <a href="#VoluntarySection">VOLUNTARY WORKS</a>&nbsp;|&nbsp;
   <a href="#TrainingSection">TRAINING/SEMINAR</a>&nbsp;|&nbsp;
   <a href="#OtherinfoSection">OTHER INFO</a>&nbsp;|&nbsp;
   <a href="#PDSQSection">PDSQ</a>&nbsp;|&nbsp;
   <a href="#ReferenceSection">REFERENCE</a>
</div><br>

<div class="mypanel">
   <div class="panel-top" id="PersonalSection">
      I. PERSONAL INFORMATION&nbsp;<a href="#Top" class="BackToTop">Back To Top</a>
   </div>
   <div class="panel-mid" id="panelPerInfo">
      <div class="row">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >LAST NAME</label><br>
                     <div class="fieldValue--" id="val_char_LastName">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >FIRST NAME</label><br>
                     <div class="fieldValue--" id="val_char_FirstName">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >MIDDLE NAME</label><br>
                     <div class="fieldValue--" id="val_char_MiddleName">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >EXT. NAME</label><br>
                     <div class="fieldValue--" id="val_char_ExtName">&nbsp;</div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >DATE OF BIRTH</label><br>
                     <div class="fieldValue--" id="val_date_BirthDate">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >PLACE OF BIRTH</label><br>
                     <div class="fieldValue--" id="val_char_BirthPlace">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-2">
                  <label class="control-label" >SEX</label><br>
                  <div class="fieldValue--" id="val_char_Sex">&nbsp;</div>
               </div>
               <div class="col-xs-2">
                  <label class="control-label" >CITIZENSHIP</label><br>
                  <div class="fieldValue--" id="val_sint_isFilipino">&nbsp;</div>
               </div>
               <div class="col-xs-2">
                  <label class="control-label" >Country (if Dual)</label><br>
                  <div class="fieldValue--" id="val_sint_CountryRefId">&nbsp;</div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >CIVIL STATUS</label><br>
                     <div class="fieldValue--" id="val_char_CivilStatus">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="form-group">
                           <label class="control-label" >HEIGHT (m)</label><br>
                           <div class="fieldValue--" id="val_deci_Height">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-6">
                        <div class="form-group">
                           <label class="control-label" >WEIGHT (kg)</label><br>
                           <div class="fieldValue--" id="val_deci_Weight">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">BLOOD TYPE</label><br>
                     <div class="fieldValue--" id="val_sint_BloodTypeRefId">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >AGENCY EMPLOYEE NO.</label><br>
                     <div class="fieldValue--" id="val_char_AgencyId">&nbsp;</div>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" >GOVT. ISSUED ID</label><br>
                     <div class="fieldValue--" id="val_char_GovtIssuedID">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                  <label class="control-label" >GOVT. ISSUED ID No.</label><br>
                  <div class="fieldValue--" id="val_char_GovtIssuedIDNo">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                  <label class="control-label" >GOVT. ISSUED ID Place</label><br>
                  <div class="fieldValue--" id="val_char_GovtIssuedIDPlace">&nbsp;</div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                  <label class="control-label" >DATE ISSUED</label><br>
                  <div class="fieldValue--" id="val_char_GovtIssuedIDDate">&nbsp;</div>
                  </div>
               </div>
            </div>
            <div class="mypanel">
               <div class="panel-top">ADDRESS</div>
               <div class="panel-mid">
                  <h4 style="color:blue;font-weight:600;">Residential Address</h4>
                  <?php bar(); ?>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">HOUSE NO.</label><br>
                           <div class="fieldValue--" id="val_char_ResiHouseNo">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">STREET</label><br>
                           <div class="fieldValue--" id="val_char_ResiStreet">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">BRGY.</label><br>
                           <div class="fieldValue--" id="val_char_ResiBrgy">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">VILLAGE / SUBD.</label><br>
                           <div class="fieldValue--" id="val_char_ResiSubd">&nbsp;</div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >COUNTRY</label><br>
                           <div class="fieldValue--" id="val_sint_ResiCountryRefId">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PROVINCE</label><br>
                           <div class="fieldValue--" id="val_sint_ResiAddProvinceRefId">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >CITY</label><br>
                           <div class="fieldValue--" id="val_sint_ResiAddCityRefId">&nbsp;</div>
                        </div>
                     </div>

                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >ZIPCODE</label><br>
                           <div class="fieldValue--" id="val_char_ResiAddZipCode">&nbsp;</div>
                        </div>
                     </div>
                  </div>
                  <h4 style="color:blue;font-weight:600;">Permanent Address</h4>
                  <?php bar(); ?>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">HOUSE NO.</label><br>
                           <div class="fieldValue--" id="val_char_PermanentHouseNo">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">STREET</label><br>
                           <div class="fieldValue--" id="val_char_PermanentStreet">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">BRGY.</label><br>
                           <div class="fieldValue--" id="val_char_PermanentBrgy">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label">VILLAGE / SUBD.</label><br>
                           <div class="fieldValue--" id="val_char_PermanentSubd">&nbsp;</div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >COUNTRY</label><br>
                           <div class="fieldValue--" id="val_sint_PermanentCountryRefId">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PROVINCE</label><br>
                           <div class="fieldValue--" id="val_sint_PermanentAddProvinceRefId">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >CITY</label><br>
                           <div class="fieldValue--" id="val_sint_PermanentAddCityRefId">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >ZIPCODE</label><br>
                           <div class="fieldValue--" id="val_char_PermanentAddZipCode">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php spacer(5); ?>
            <div class="mypanel">
               <div class="panel-top">CONTACT's</div>
               <div class="panel-mid">
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >TELEPHONE NO.</label><br>
                           <div class="fieldValue--" id="val_char_TelNo">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >MOBILE NO</label><br>
                           <div class="fieldValue--" id="val_char_MobileNo">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >E-MAIL ADDRESS (if any)</label><br>
                           <div class="fieldValue--" id="val_char_EmailAdd">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php spacer(5); ?>
            <div class="mypanel">
               <div class="panel-top">ID's</div>
               <div class="panel-mid">
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >GSIS ID NO.</label><br>
                           <div class="fieldValue--" id="val_char_GSIS">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PAGIBIG ID NO.</label><br>
                           <div class="fieldValue--" id="val_char_PAGIBIG">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >PHILHEALTH NO.</label><br>
                           <div class="fieldValue--" id="val_char_PHIC">&nbsp;</div>
                        </div>
                     </div>
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >TIN NO.</label><br>
                           <div class="fieldValue--" id="val_char_TIN">&nbsp;</div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-3">
                        <div class="form-group">
                           <label class="control-label" >SSS NO.</label><br>
                           <div class="fieldValue--" id="val_char_SSS">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="mypanel">
   <div class="panel-top" id="FamilySection">
      II. FAMILY BACKGROUND&nbsp;<a href="#Top" class="BackToTop">Back To Top</a>
   </div>
   <div class="panel-mid" id="panelFamBG">
      <div class="panel-top">
         SPOUSE's INFORMATION
      </div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputSLName">LAST NAME:</label>
                  <br><div class="fieldValue--" id="val_char_SpouseLastName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputSFName">FIRST NAME:</label>
                  <br><div class="fieldValue--" id="val_char_SpouseFirstName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputSMName">MIDDLE NAME:</label>
                  <br><div class="fieldValue--" id="val_char_SpouseMiddleName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label">EXT. NAME</label>
                  <br><div class="fieldValue--" id="val_char_SpouseExtName">&nbsp;</div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label">MOBILE NO.:</label>
                  <br><div class="fieldValue--" id="val_char_SpouseMobileNo">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputOccupation">OCCUPATION:</label>
                  <br><div class="fieldValue--" id="val_sint_OccupationsRefId">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label">EMPLOYER's NAME</label>
                  <br><div class="fieldValue--" id="val_char_EmployersName">&nbsp;</div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-6">
               <div class="form-group">
                  <label class="control-label">BUSINESS ADDRESS</label>
                  <br><div class="fieldValue--" id="val_char_BusinessAddress">&nbsp;</div>
               </div>
            </div>

         </div>
      </div>
      <div class="panel-top">
         FATHER's INFORMATION
      </div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputFLName">LAST NAME:</label>
                  <br><div class="fieldValue--" id="val_char_FatherLastName">&nbsp;</div>
               </div>
            </div>

            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputFFName">FIRST NAME:</label>
                  <br><div class="fieldValue--" id="val_char_FatherFirstName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputFMName">MIDDLE NAME:</label>
                  <br><div class="fieldValue--" id="val_char_FatherMiddleName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputFXName">EXT NAME:</label>
                  <br><div class="fieldValue--" id="val_char_FatherExtName">&nbsp;</div>
               </div>
            </div>
         </div>
      </div>
      <?php spacer(5); ?>
      <div class="panel-top">
         MOTHER's INFORMATION (Maiden Name)
      </div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputMLName">LAST NAME:</label>
                  <br><div class="fieldValue--" id="val_char_MotherLastName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputMFName">FIRST NAME:</label>
                  <br><div class="fieldValue--" id="val_char_MotherFirstName">&nbsp;</div>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputMMName">MIDDLE NAME:</label>
                  <br><div class="fieldValue--" id="val_char_MotherMiddleName">&nbsp;</div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="mypanel">
   <?php spacer(5); ?>
   <div class="panel-top" id="ChildrenSection">CHILDREN'S INFORMATION&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="contentChild"></div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="EducSection">III. EDUCATIONAL BACKGROUND&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <?php spacer(5); ?>
   <div class="panel-top">ELEMENTARY</div>
   <div class="panel-mid" id="elem_content"></div>
   <?php spacer(5); ?>
   <div class="panel-top">SECONDARY</div>
   <div class="panel-mid" id="secondary_content"></div>
   <?php spacer(5); ?>
   <div class="panel-top">VOCATIONAL</div>
   <div class="panel-mid" id="Vocationalcontent"></div>
   <?php spacer(5); ?>
   <div class="panel-top">COLLEGE</div>
   <div class="panel-mid" id="college_content"></div>
   <?php spacer(5); ?>
   <div class="panel-top">GRADUATE STUDIES</div>
   <div class="panel-mid" id="GradStudiescontent"></div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="EligibilitySection">IV. ELIGIBILITY&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="eligibilitycontent"></div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="WorkExpSection">V. WORK EXPERIENCE&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="workexpcontent">
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="VoluntarySection">VI. VOLUNTARY WORK&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="voluntarycontent"></div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="TrainingSection">VII. TRAINING PROGRAMS&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="trainingcontent"></div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="OtherinfoSection">VIII. OTHER INFORMATION&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="otherinfocontent"></div>
</div>
<?php spacer(5); ?>
<div class="mypanel">
   <div class="panel-top" id="PDSQSection">
      IX. PDS QUESTION&nbsp;<a href="#Top" class="BackToTop">Back To Top</a>
   </div>
   <div class="panel-mid" id="panelPDSQ">
   <div class="row">
      <?php spacer(5); ?>
      <div class="col-xs-6">
         <div class="panel-top">
            34. Are you related by consanguinity or affinity to the appointing or recommending authority, or to the chief of bureau or office or to the person who has immediate supervision over you in the Office, Bureau or Department where you will be apppointed,
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. within the third degree?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q1aexp">&nbsp;</div><br>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. within the fourth degree (for Local Government Unit - Career Employees)?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q1bexp">&nbsp;</div><br>
               </div>
            </div>
         </div>

         <div class="panel-top margin-top">
            35.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been found guilty of any administrative offense?<br>
                  
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q2aexp">&nbsp;</div><br>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. Have you been criminally charged before any court?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q2bexp">&nbsp;</div><br>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-4">
                  Date Filed:<br>
                  <div class="fieldValue--" id="val_date_Q2DateFiled">&nbsp;</div>
               </div>
               <div class="col-xs-8">
                  Status of Case/s:<br>
                  <div class="fieldValue--" id="val_char_Q2CaseStatus">&nbsp;</div>
               </div>
            </div>

         </div>
         <div class="panel-top margin-top">
            36.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been convicted of any crime or violation of any law, decree, ordinance or regulation by any court or tribunal?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q3aexp">&nbsp;</div><br>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-6">
         <div class="panel-top">
            37.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been separated from the service in any of the following modes: resignation, retirement, dropped from the rolls, dismissal, termination, end of term, finished contract or phased out (abolition) in the public or private sector?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q4aexp">&nbsp;</div><br>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            38.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you ever been a candidate in a national or local election held within the last year (except Barangay election)?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q5aexp">&nbsp;</div><br>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. Have you resigned from the government service during the three (3)-month period before the last election to promote/actively campaign for a national or local candidate?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q5bexp">&nbsp;</div><br>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            39.
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Have you acquired the status of an immigrant or permanent resident of another country?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q6aexp">&nbsp;</div><br>
               </div>
            </div>
         </div>
         <div class="panel-top margin-top">
            40. Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277); and (c) Solo Parents Welfare Act of 2000 (RA 8972), please answer the following items:
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-xs-12">
                  a. Are you a member of any indigenous group?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, give details
                  <div class="fieldValue--" id="val_char_Q7aexp">&nbsp;</div><br>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  b. Are you a person with disability?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, please specify ID no.<br>&nbsp;
                  <div class="fieldValue--" id="val_char_Q7bexp">&nbsp;</div><br>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  c. Are you a solo parent?<br>
                  
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  If YES, please specify ID no.<br>&nbsp;
                  <div class="fieldValue--" id="val_char_Q7cexp">&nbsp;</div><br>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
</div>
<?php spacer(5);?>
<div class="mypanel">
   <div class="panel-top" id="ReferenceSection">X. REFERENCE&nbsp;<a href="#Top" class="BackToTop">Back To Top</a></div>
   <div class="panel-mid" id="refcontent"></div>
   <div class="panel-bottom"></div>
</div>

