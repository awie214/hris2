
                                 <div class="container-fluid">
                                    <div>
                                          <table class="table" width="100%" style="clear:both;">
                                             <tr>
                                                <td class="txt-right">NAME:</td>
                                                <td class="txt-center">
                                                   <div style="border-bottom:2px solid #000"><b><?php echo $row["LastName"]; ?></b></div>
                                                   <span class='rptFooter'>(Surname)</span>
                                                </td>
                                                <td class="txt-center">
                                                   <div style="border-bottom:2px solid #000"><b><?php echo $row["FirstName"]; ?></b></div>
                                                   <span class='rptFooter'>(Given Name)</span>
                                                </td>
                                                <td class="txt-center">
                                                   <div style="border-bottom:2px solid #000"><b><?php echo $row["MiddleName"]; ?></b></div>
                                                   <span class='rptFooter'>(Middle Name)</span>
                                                </td>
                                                <td>
                                                   <span class='rptFooter'>
                                                      (If married woman,give also full maiden name)
                                                   </span>
                                                </td>
                                             </tr>

                                             <tr>
                                                <td class="txt-right">BIRTH:</td>
                                                <td class="txt-center">
                                                   <div style="border-bottom:2px solid #000"><b><?php echo $row["BirthDate"]; ?></b></div>
                                                   <span class='rptFooter'>(Date)</span>
                                                </td>
                                                <td class="txt-center">
                                                   <div style="border-bottom:2px solid #000"><b><?php echo $row["BirthPlace"]; ?></b></div>
                                                   <span class='rptFooter'>(Place)</span>
                                                   
                                                </td>
                                                <td colspan=2>
                                                   <span class='rptFooter'>
                                                      Date herein should be checked from birth or baptismal certificate or some other reliable documents.
                                                   </span>
                                                </td>
                                             </tr>
                                          </table>
                                          <br>

                                          <div class="row">
                                             <div class="col-sm-12 txt-center">
                                                <p style="text-align:justify">
                                                This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities.
                                                </p>
                                             </div>
                                          </div>
                                          <br>

                                          <table class="rptBody table" border=1>
                                             <thead>
                                                <tr class="txt-center">
                                                   <th colspan=2 rowspan=2>
                                                      SERVICE<br>
                                                      <span class='rptFooter'>
                                                         (Includsive Dates)
                                                      </span>
                                                   </th>
                                                   <th colspan=4>
                                                      RECORDS OF APPOINTMENT
                                                   </th>
                                                   <th rowspan=3>Leave<br>Absent<br>w/o Pay</th>
                                                   <th rowspan=3>Remarks<br>Separation<br>Reference</th>
                                                </tr>
                                                <tr class="txt-center">
                                                   <th rowspan=2>DESIGNATIONS</th>
                                                   <th rowspan=2>STATUS<br>(1)</th>
                                                   <th rowspan=2>SALARY<br>(2)</th>
                                                   <th rowspan=2>Station/Place<br>of Assignment</th>
                                                </tr>
                                                <tr class="txt-center">
                                                   <th width="90px">FROM</th>
                                                   <th width="90px">TO</th>
                                                </tr>
                                             </thead>   
                                          <?php
                                          while ($rowServiceRecords = mysqli_fetch_assoc($rsServiceRecords)) {
                                          ?>
                                             <tr>
                                                <td class="txt-center">
                                                   <?php 
                                                      echo CValue($rowServiceRecords["EffectivityDate"]); 
                                                   ?>
                                                </td>
                                                <td class="txt-center">
                                                   <?php 
                                                      echo CValue($rowServiceRecords["ExpiryDate"]); 
                                                   ?>
                                                </td>
                                                <td>
                                                   <?php 
                                                      dispBlank(getRecord("Position",$rowServiceRecords["PositionRefId"],"Name")); 
                                                   ?>
                                                </td>
                                                <td>
                                                   <?php 
                                                      dispBlank(getRecord("EmpStatus",$rowServiceRecords["EmpStatusRefId"],"Name")); 
                                                   ?>
                                                </td>
                                                <td class="txt-right">
                                                   <?php 
                                                      dispNumFormat($rowServiceRecords["SalaryAmount"]); 
                                                   ?>
                                                </td>
                                                <td class="text-center">
                                                   <?php 
                                                      dispBlank(getRecord("Company",$rowServiceRecords["CompanyRefId"],"Code")); 
                                                   ?>
                                                </td>
                                                <td>
                                                   <?php 
                                                      CValue($rowServiceRecords["LWOP"]); 
                                                   ?>
                                                </td>
                                                <td>
                                                   <?php
                                                      echo CValue($rowServiceRecords["LWOPDate"])." / ";
                                                      dispBlank($rowServiceRecords["Cause"]); 
                                                   ?>
                                                </td>
                                             </tr>
                                          <?php
                                          }
                                          ?>
                                          <tr class="txt-center">
                                             <td colspan=8>--- NOTHING FOLLOWS ---</td>
                                          </tr>
                                          </table>
                                          <div class="row">
                                             <div class="col-sm-12">
                                                <p style="text-align:justify">
                                                   With Transition Allowance with the Amount of : P<br>
                                                   Issued in compliance with Executive Order No. 54 dated August 10, 1984, and in accordance with Circular, No. 58, dated August 10, 1954 of the System.
                                                </p>
                                             </div>
                                          </div>
                                          <br><br>
                                          <div class="row">
                                             <div class="col-sm-2 txt-right">DATE:</div>
                                             <div class="col-sm-2" style="border-bottom:2px solid #000"><?php echo date("Y-m-d",time()); ?></div>
                                             <div class="col-sm-2">&nbsp;</div>
                                             <div class="col-sm-3 txt-right">CERTIFIED CORRECT:</div>
                                             <div class="col-sm-2" style="border-bottom:0px solid #000">&nbsp;</div>
                                             <div class="col-sm-1 txt-center"></div>
                                          </div>
                                       
                                    </div>
                                 </div>