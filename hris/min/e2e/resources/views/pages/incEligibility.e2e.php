<div class="mypanel">
   <div class="panel-top" for="Eligibility">
      <!--
      <div class="row txt-center">
         <div class="col-xs-4">CAREER SERVICE/RA 1080 (BOARD/BAR)<br>UNDER SPECIAL LAWS/CES/CSEE</div>
         <div class="col-xs-2">RATING</div>
         <div class="col-xs-2">DATE OF EXAMINATION/<br>CONFERMENT</div>
         <div class="col-xs-4">PLACE OF EXAMINATION /<br>CONFERMENT</div>
      </div>
      
      <div class="row txt-center">

         <div class="col-xs-2">LICENSE NUMBER<br>(if applicable)</div>
         <div class="col-xs-2">DATE OF RELEASE</div>
      </div>
      -->
   </div>
   <div class="panel-mid-litebg" id="Eligibility">
      <?php

         $createEntry = 0;
         if (getvalue("hUserGroup") == "COMPEMP") {
            $rs = SelectEach("employeeselegibility","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId");
            $j = 0;
            if ($rs) {
               $createEntry = mysqli_num_rows($rs);
            } else {
               $createEntry = 1;
            }
         } else {
            $createEntry = 1;
         }
         $disabled = "disabled";
         for ($j=1;$j<=$createEntry;$j++) {

      ?>

         <div id="EntryEligib_<?php echo $j; ?>" class="entry201">
            <input type="checkbox" id="Eligib_<?php echo $j; ?>" name="chkEligib_<?php echo $j; ?>" class="enabler-- eligibility_fpreview" refid="" 
            fldName="sint_CareerServiceRefId_<?php echo $j; ?>,
            sint_Rating_<?php echo $j; ?>,
            date_ExamDate_<?php echo $j; ?>,
            char_ExamPlace_<?php echo $j; ?>,
            char_LicenseNo_<?php echo $j; ?>,
            date_LicenseReleasedDate_<?php echo $j; ?>" 
            idx="<?php echo $j; ?>" unclick="CancelAddRow">
            <input type="hidden" name="eligibilityRefId_<?php echo $j;?>" value="">
            <label class="btn-cls2-sea" for="Eligib_<?php echo $j; ?>"><b>EDIT ELIGIBILITY #<?php echo $j; ?></b></label>

            <div class="row margin-top">
               <div class="col-sm-3">
                  <label>Career Service/RA 1080 (Write in Full):</label><br>
                  <?php
                     createSelect("CareerService",
                                  "sint_CareerServiceRefId_".$j,
                                  "",100,"Name","Select Career Service",$disabled);
                  ?>
               </div>
               <div class="col-sm-2">
                  <label>Rating:</label><br>
                  <input type="text" class="form-input saveFields-- number--" placeholder="RATING"
                  name="sint_Rating_<?php echo $j; ?>" <?php echo $disabled; ?> onblur="checkRating(<?php echo $j; ?>)">
               </div>
               <div class="col-sm-2">
                  <label>Exam Date:</label><br>
                  <input type="text" class="form-input date-- saveFields-- valDate--" placeholder="EXAM DATE"
                  name="date_ExamDate_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
               </div>
               <div class="col-sm-4">
                  <label>Exam Place:</label><br>
                  <input type="text" class="form-input saveFields-- uCase--" placeholder="PLACE OF EXAMINATION CONFERMENT"
                  name="char_ExamPlace_<?php echo $j; ?>" <?php echo $disabled; ?>>
               </div>
               <div class="col-sm-3">
                  <label>License No:</label><br>
                  <input type="text" class="form-input saveFields--" placeholder="LICENSE NUMBER (if applicable)"
                  name="char_LicenseNo_<?php echo $j; ?>" <?php echo $disabled; ?>>
               </div>
               <div class="col-sm-2">
                  <label>License Date:</label><br>
                  <input type="text" class="form-input date-- saveFields--" placeholder="DATE OF RELEASE"
                  name="date_LicenseReleasedDate_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
               </div>

            </div>
         
<!--
            <div class="row margin-top">
               <div class="col-xs-4 txt-center">
                  <?php
                     createSelect("CareerService",
                                  "sint_CareerServiceRefId_".$j,
                                  "",100,"Name","Select Career Service",$disabled);
                  ?>
               </div>
               <div class="col-xs-2 txt-center">
                  <input type="text" class="form-input saveFields-- number--" placeholder="RATING"
                  name="sint_Rating_<?php echo $j; ?>" <?php echo $disabled; ?> onblur="checkRating(<?php echo $j; ?>)">
               </div>
               <div class="col-xs-2 txt-center">
                  <input type="text" class="form-input date-- saveFields-- valDate--" placeholder="EXAM DATE"
                  name="date_ExamDate_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
               </div>
               <div class="col-xs-4 txt-center">
                  <input type="text" class="form-input saveFields-- uCase--" placeholder="PLACE OF EXAMINATION CONFERMENT"
                  name="char_ExamPlace_<?php echo $j; ?>" <?php echo $disabled; ?>>
               </div>
            </div>

            <div class="row margin-top">
               <div class="col-xs-4 txt-center">
                  <input type="text" class="form-input saveFields--" placeholder="LICENSE NUMBER (if applicable)"
                  name="char_LicenseNo_<?php echo $j; ?>" <?php echo $disabled; ?>>
               </div>
               <div class="col-xs-2"></div>
               <div class="col-xs-2 txt-center">
                  <input type="text" class="form-input date-- saveFields--" placeholder="DATE OF RELEASE"
                  name="date_LicenseReleasedDate_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
               </div>
            </div>
-->

         </div>
      <?php } ?>
   </div>
   <div class="panel-bottom bgSilver"><a href="javascript:void(0);" class="addRow" id="addRowEligibility">Add Row</a></div>


</div>
<script type="text/javascript">

   //$("#Eligibility .newDataLibrary").hide();
   //$("#Eligibility select").css("width","100%");
   $("#Eligibility select").addClass("mandatory").not("[name*='date_LicenseReleasedDate_'], [name*='date_ExamDate_']");
   $("#Eligibility .saveFields--").attr("tabname","Eligibility");
   function checkRating(idx) {
      var val = $("[name='sint_Rating_"+idx+"']").val();
      if (val.indexOf(".")) {
         val_arr = val.split(".")[1];
         if (parseInt(val_arr.length) > 2) {
            alert("too many decimal places");
            $("[name='sint_Rating_"+idx+"']").val("");
            $("[name='sint_Rating_"+idx+"']").focus();
         }
      }
   }
</script>