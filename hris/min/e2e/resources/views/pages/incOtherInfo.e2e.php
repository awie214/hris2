<div class="mypanel">
   <div class="panel-top" for="otherinfo">
      <div class="row">
         <div class="col-xs-4 txt-center">
            SPECIAL SKILLS/HOBBIES
         </div>
         <div class="col-xs-4 txt-center">
            NON-ACADEMIC DISTINCTION/RECOGNITION<BR>
            (Write in full)
         </div>
         <div class="col-xs-4 txt-center">
            MEMBERSHIP IN ASSOCIATION/ORGANIZATION<br>
            (Write in full)
         </div>
      </div>
   </div>
   <div class="panel-mid-litebg" id="otherinfo">
      <?php
      $createEntry = 0;
         if (getvalue("hUserGroup") == "COMPEMP") {
            $rs = SelectEach("employeesotherinfo","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY RefId");
            $j = 0;
            if ($rs) {
               $createEntry = mysqli_num_rows($rs);
            } else {
               $createEntry = 1;
            }
         } else {
            $createEntry = 1;
         }
         $disabled = "disabled";
         $createEntry = 5;
         for ($j=1;$j<=$createEntry;$j++) {
      ?>
            <div id="EntryOtherInfo_<?php echo $j; ?>" class="entry201">
               <input type="checkbox" id="OtherInfo_<?php echo $j; ?>" name="chkOtherInfo_<?php echo $j; ?>" class="enabler-- other_fpreview" refid="" 
               fldName="char_Skills_<?php echo $j; ?>,char_Recognition_<?php echo $j; ?>,char_Affiliates_<?php echo $j; ?>" idx="<?php echo $j; ?>" unclick="CancelAddRow">
               <input type="hidden" name="otherinfoRefId_<?php echo $j; ?>" value="">
               <label for="OtherInfo_<?php echo $j; ?>" class="btn-cls2-sea"><b>EDIT OTHER INFO #<?php echo $j; ?></b></label>
               <div class="row margin-top">
                  <div class="col-xs-4 txt-center">
                     <input type="text" class="form-input saveFields-- uCase--" tabname="Other Info" name="char_Skills_<?php echo $j; ?>" <?php echo $disabled; ?>/>
                  </div>
                  <div class="col-xs-4 txt-center">
                     <input type="text" class="form-input saveFields-- uCase--" tabname="Other Info" name="char_Recognition_<?php echo $j; ?>" <?php echo $disabled; ?>/>
                  </div>
                  <div class="col-xs-4 txt-center">
                     <input type="text" class="form-input saveFields-- uCase--" tabname="Other Info" name="char_Affiliates_<?php echo $j; ?>" <?php echo $disabled; ?>/>
                  </div>
               </div>
            </div>
      <?php
         }
      ?>
   </div>
   <div class="panel-bottom"></div>
   <!--
   <div class="panel-bottom bgSilver"><a href="javascript:void(0);" class="addRow" id="addRowOtherInfo">Add Row</a></div>
   -->
</div>