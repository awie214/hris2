<?php
   /*---Parameters---*/
   $REFID               = getvalue("txtRefId");
   $p_LastName          = getvalue("txtLName");
   $p_FirstName         = getvalue("txtFName");
   $p_MiddleName        = getvalue("txtMidName");
   $p_CivilStatus       = getvalue("drpCvlStat");
   $p_Sex               = getvalue("Gender");
   $p_EmpStat           = getvalue("drpEmpStat");
   $p_DOBFrom           = getvalue("txtDOBFrom");
   $p_DOBTo             = getvalue("txtDOBTo");
   $p_HeightFrom        = getvalue("txtHeightFrom");
   $p_HeightTo          = getvalue("txtHeightTo");
   $p_WeightFrom        = getvalue("txtWeightFrom");
   $p_WeightTo          = getvalue("txtWeightTo");
   $p_RProvince         = getvalue("drpRProvince");
   $p_RCity             = getvalue("drpRCity");

   if ($dbg) {
      echo "<ol><li>$p_LastName</li>";
      echo "<li>$p_FirstName</li>";
      echo "<li>$p_MiddleName</li>";
      echo "<li>$p_CivilStatus</li>";
      echo "<li>$p_Sex</li>";
      echo "<li>$p_EmpStat</li>";
      echo "<li>$p_DOBFrom</li>";
      echo "<li>$p_DOBTo</li>";
      echo "<li>$p_HeightFrom</li>";
      echo "<li>$p_HeightTo</li>";
      echo "<li>$p_WeightFrom</li>";
      echo "<li>$p_WeightTo</li>";
      echo "<li>$p_RProvince</li>";
      echo "<li>$p_RCity</li></ol>";
   }
   /*=============================================*/
   $CivilStatus = "";
   $table = "employees";
   $whereClause = "where CompanyRefId = ".$CompanyId." AND BranchRefId = ".$BranchId;
   $searchCriteria = "";
   $_male = $_female = $_sex_undef = $_single = $_married = $_annulled = $_widowed = 0;
   $_separated = $_others = $_civil_undef = $total_sex = $total_civil = 0;
   if ($REFID != ""){
      $whereClause .= " and RefId = '".$REFID."%'";
      $searchCriteria .= "EMPLOYEES REFID ".$REFID;
   }
   if ($p_LastName != "") {
      $whereClause .= " and LastName LIKE '".$p_LastName."%'";
      $searchCriteria .= "LAST NAME begins with ".$p_LastName;
   }
   if ($p_FirstName != "") {
      $whereClause .= " and FirstName LIKE '".$p_FirstName."%'";
      if ($searchCriteria == "")
         $searchCriteria .= "FIRST NAME begins with ".$p_FirstName;
      else
         $searchCriteria .= "|FIRST NAME begins with ".$p_FirstName;
   }
   if ($p_MiddleName != "") {
      $whereClause .= " and MiddleName LIKE '".$p_MiddleName."%'";
      if ($searchCriteria == "")
         $searchCriteria .= "MIDDLE NAME begins with ".$p_MiddleName;
      else
         $searchCriteria .= "|MIDDLE NAME begins with ".$p_MiddleName;
   }
   if ($p_CivilStatus != "") {
      $whereClause .= " and CivilStatus = '$p_CivilStatus'";
      switch ($p_CivilStatus) {
         case "Si":
            if ($searchCriteria == "")
               $searchCriteria .= "CIVIL STATUS is Single";
            else
               $searchCriteria .= "|CIVIL STATUS is Single";
         break;
         case "Ma":
            if ($searchCriteria == "")
               $searchCriteria .= "CIVIL STATUS is Married";
            else
               $searchCriteria .= "|CIVIL STATUS is Married";
         break;
         case "An":
            if ($searchCriteria == "")
               $searchCriteria .= "CIVIL STATUS is Annulled";
            else
               $searchCriteria .= "|CIVIL STATUS is Annulled";
         break;
         case "Wi":
            if ($searchCriteria == "")
               $searchCriteria .= "CIVIL STATUS is Widowed";
            else
               $searchCriteria .= "|CIVIL STATUS is Widowed";
         break;
         case "Se":
            if ($searchCriteria == "")
               $searchCriteria .= "CIVIL STATUS is Separated";
            else
               $searchCriteria .= "|CIVIL STATUS is Separated";
         break;
         case "Ot":
            if ($searchCriteria == "")
               $searchCriteria .= "CIVIL STATUS is Other";
            else
               $searchCriteria .= "|CIVIL STATUS is Other";
         break;
      }
   }
   if ($p_Sex != "") {
      $whereClause .= " and Sex = '$p_Sex'";
      switch ($p_Sex) {
         case "M":
            if ($searchCriteria == "")
               $searchCriteria .= "SEX Male";
            else
               $searchCriteria .= "|SEX Male";
         break;
         case "F":
            if ($searchCriteria == "")
               $searchCriteria .= "SEX Female";
            else
               $searchCriteria .= "|SEX Female";
         break;
      }
   }
   if ($p_DOBFrom != "" && $p_DOBTo != "") {
      $whereClause .= " and BirthDate BETWEEN '$p_DOBFrom' AND '$p_DOBTo'";
      if ($searchCriteria == "")
         $searchCriteria .= "Birth Date BETWEEN '$p_DOBFrom' AND '$p_DOBTo'";
      else
         $searchCriteria .= "|Birth Date BETWEEN '$p_DOBFrom' AND '$p_DOBTo'";
   }
   if ($p_HeightFrom != ""){
      $whereClause .= " and Height >= $p_HeightFrom";
      if ($searchCriteria == "")
         $searchCriteria .= "Height Above or Equal $p_HeightFrom";
      else
         $searchCriteria .= "|Height Above or Equal $p_HeightFrom";
   }
   if ($p_HeightTo != ""){
      $whereClause .= " and Height <= $p_HeightTo";
      if ($searchCriteria == "")
         $searchCriteria .= "Height Below or Equal $p_HeightTo";
      else
         $searchCriteria .= "|Height Below or Equal $p_HeightTo";
   }
   if ($p_WeightFrom != ""){
      $whereClause .= " and Weight >= $p_WeightFrom";
      if ($searchCriteria == "")
         $searchCriteria .= "Weight Above or Equal $p_WeightFrom";
      else
         $searchCriteria .= "|Weight Above or Equal $p_WeightFrom";
   }
   if ($p_WeightTo != ""){
      $whereClause .= " and Weight <= $p_WeightTo";
      if ($searchCriteria == "")
         $searchCriteria .= "Weight Below or Equal $p_WeightTo";
      else
         $searchCriteria .= "|Weight Below or Equal $p_WeightTo";
   }
   if ($p_RProvince > 0){
      $whereClause .= " and ResiAddProvinceRefId = $p_RProvince";
      if ($searchCriteria == "")
         $searchCriteria .= "Province $p_RProvince";
      else
         $searchCriteria .= "|Province $p_RProvince";
   }
   if ($p_RCity > 0){
      $whereClause .= " and ResiAddCityRefId = $p_RCity";
      if ($searchCriteria == "")
         $searchCriteria .= "City $p_RCity";
      else
         $searchCriteria .= "|City $p_RCity";
   }

   if (getvalue("drpSortBy")!="") {
      switch (getvalue("drpSortBy")) {
         case "LastName":
            $whereClause .= " order by LastName, FirstName";
         break;
         case "FirstName":
            $whereClause .= " order by FirstName, LastName";
         break;
         case "BirthDate":
            $whereClause .= " order by BirthDate, LastName";
         break;
         case "Height":
            $whereClause .= " order by Height, LastName";
         break;
         case "Weight":
            $whereClause .= " order by Weight, LastName";
         break;
      }
   }
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);

   if ($dbg) {
      echo $whereClause;
   }
?>