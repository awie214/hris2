<script>
   $(document).ready(function () {
      $(".newDataLibrary").hide();
      $(".createSelect--").css("width","100%");
      $("#employmentSummary [class*='saveFields--']").prop("disabled",true);
      $("#employmentSummary .enabler--").click(function () {
         if ($(this).is(":checked")) {
            $("#employmentSummary [class*='saveFields--']").prop("disabled",false);
         } else {
            $("#employmentSummary [class*='saveFields--']").prop("disabled",true);
         }
      });
   });
</script>
<div class="mypanel" id="employmentSummary" style="padding:5px;">
   <div class="row">
       <input type="checkbox" class="enabler--" name="chkEnableemploymentSummary">
      <label id="enable">Enable Fields</label>
   </div>
   <div class="row margin-top">
      <div class="col-xs-4">
         <div class="row" style="padding:5px;border:1px solid black;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Employment Status:</label>
                  </div>
                  <div class="col-xs-6">
                     <select name="EmploymentStatus" class="saveFields-- form-input">
                        <option></option>
                        <option>Permanent</option>
                        <option>Contractual</option>
                        <option>Casual</option>
                        <option>Temporary</option>
                        <option>Job Order</option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Employee No.:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="EmployeeNo" class="saveFields-- form-input">
                  </div>
               </div>
               <!--
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <label>Policy No.</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" class="form-input">
                  </div>
               </div>
               -->
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>BP No.:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="BpNo" class="saveFields-- form-input">
                  </div>
               </div>
            </div>
         </div>
         <div class="row margin-top" style="padding:5px;border:1px solid black;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Position Item No.</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="PositionItemNo" class="saveFields-- form-input" value="<?php ?>">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Position</label>
                  </div>
                  <div class="col-xs-6">
                     <?php 
                           $ftable = "Position";
                           createSelect($ftable,$ftable."RefId","",100,"Name","Select ".$ftable,"");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Salary Grade:</label>
                  </div>
                  <div class="col-xs-6">
                     <?php 
                           $ftable = "SalaryGrade";
                           createSelect($ftable,$ftable."RefId","",100,"Name","Select Salary Grade","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Job Grade:</label>
                  </div>
                  <div class="col-xs-6">
                     <?php $ftable = "JobGrade";
                           createSelect($ftable,$ftable."RefId","",100,"Name","Select Salary Grade","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Step Increment:</label>
                  </div>
                  <div class="col-xs-6">
                     <?php $ftable = "stepincrement";
                           createSelect($ftable,$ftable."RefId","",100,"Name","Select Salary Step","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Responsibility Center:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="ResponsibilityCenter" class="saveFields-- form-input">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-4" style="padding-left:10px;">
         <div class="row" style="padding:5px;border:1px solid black;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-6 txt-right" title="Date Hired">
                     <label>First Day of Service:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="FirstDayofService" 
                           class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 txt-right">
                     <label>Assumption to Duty:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="AssumptiontoDuty" class="saveFields-- form-input date--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 txt-right">
                     <label>Resigned/Retired:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="ResignedRetired" class="saveFields-- form-input date--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 txt-right">
                     <label>Re-employment:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="Reemployment" class="saveFields-- form-input date--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 txt-right">
                     <label>Transfer:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="Transfer" class="saveFields-- form-input date--" value="">
                  </div>
               </div>
            </div>
         </div>
         <div class="row margin-top" style="padding:5px;border:1px solid black;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12 txt-center">
                     <label>For Job Order/Contractual:</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Assumption:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="JOAssumption" class="saveFields-- form-input date--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Resigned/Retired:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="JOResignedRetired" class="saveFields-- form-input date--" value="">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-4" style="padding-left:10px;">
         <div class="row" style="padding:5px;border:1px solid black;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Pay Period:</label>
                  </div>
                  <div class="col-xs-6">
                     <?php 
                        $ftable = "PayPeriod";
                        createSelect($ftable,$ftable."RefId","",100,"Name","Select Pay Period","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Pay Rate:</label>
                  </div>
                  <div class="col-xs-6 ">
                     <?php 
                        $ftable = "PayRate";
                        createSelect($ftable,$ftable."RefId","",100,"Name","Select Pay Rate","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Annual Rate:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="AnnualRate" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Monthly Rate:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="MonthlyRate" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Weekly Rate:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="WeeklyRate" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Daily Rate:</label>
                  </div>
                  <div class="col-xs-6 label">
                     <input type="text" name="DailyRate" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Wage Region:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="WageRegion" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Tax Status:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="TaxStatus" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Exemptions:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="Exemptions" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>ATM Account No.:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="ATMAccountNo" class="saveFields-- form-input" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Bank:</label>
                  </div>
                  <div class="col-xs-6 label">
                     <?php
                        createSelect("Bank",
                         "BankRefId",
                         "",100,"Name","Select Bank","");
                     ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-8" style="padding:5px;border:1px solid black;">
         <div class="row margin-top">
            <div class="col-xs-4">
               <label>Government Policy/Contribution:</label>
            </div>
            <div class="col-xs-4">
               <label>Policy</label>
            </div>
            <div class="col-xs-4">
               <label>Contribution</label>
            </div>
         </div>
         <hr>
         <div class="row margin-top">
            <div class="col-xs-4 label">
               <label>GSIS Policy:</label>
            </div>
            <div class="col-xs-4">
               <?php
                  createSelect("gsispolicy",
                   "gsispolicyRefId",
                   "",100,"Name","Select GSIS Policy","");
               ?>
            </div>
            <div class="col-xs-4">
               <input type="text" name="GSISContribution" class="saveFields-- form-input number--" value="">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-4 label">
               <label>Philhealth Policy:</label>
            </div>
            <div class="col-xs-4">
               <?php
                  createSelect("philhealthpolicy",
                   "philhealthpolicyRefId",
                   "",100,"Name","Select PHIC Policy","");
               ?>
            </div>
            <div class="col-xs-4">
               <input type="text" class="saveFields-- form-input number--" value="" name="PhilhealthContribution">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-4 label">
               <label>Pagibig Policy:</label>
            </div>
            <div class="col-xs-4">
               <?php
                  createSelect("pagibigpolicy",
                   "pagibigpolicyRefId",
                   "",100,"Name","Select Pagibig Policy","");
               ?>
            </div>
            <div class="col-xs-4">
               <input type="text" class="saveFields-- form-input number--" value="" name="PagibigContribution">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-4 label">
               <label>Tax Policy:</label>
            </div>
            <div class="col-xs-4">
               <?php
                  createSelect("taxpolicy",
                   "taxpolicyRefId",
                   "",100,"Name","Select Tax Policy","");
               ?>
            </div>
            <div class="col-xs-4">
               <input type="text" class="saveFields-- form-input number--" value="" name="TaxContribution">
            </div>
         </div>
      </div>
      <div class="col-xs-4" style="padding:5px;padding-left:10px;">
         <div class="row" style="padding:5px;border:1px solid black;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Union Dues:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="UnionDues" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 label">
                     <label>Provident Fund:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="ProvidentFund" class="saveFields-- form-input number--" value="">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
