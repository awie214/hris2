<script>
   $(document).ready(function () {
      $(".newDataLibrary").hide();
      $(".createSelect--").css("width","100%");
   });
</script>
<div id="empsalaryInfo">
   <div class="mypanel">
      <div class="panel-top">List</div>
      <div class="panel-mid" style="max-height:200px;overflow:auto;">

         <div class="row">
            <?php
               $rs = SelectEach("pms_employeessalary","");
               if ($rs) {
                  $recordNum = mysqli_num_rows($rs);
                  echo 
                  '<table border=1 width="99%" align="center">
                  <tr>
                     <th class="txt-center">Effectivity Date</th>
                     <th class="txt-center">Description</th>
                     <th class="txt-center">Old Rate</th>
                     <th class="txt-center">New Rate</th>
                  </tr>';
                  while ($row = mysqli_fetch_array($rs)) {
            ?>
                     <tr>
                        <td><?php echo CValue($row["EffectivityDate"]); ?></td>
                        <td><?php echo CValue($row["Description"]); ?></td>
                        <td><?php echo CValue($row["OldRate"]); ?></td>
                        <td><?php echo CValue($row["NewRate"]); ?></td>
                     </tr>
            <?php
                  }
                  echo '</table>';
               } 
               else {
            ?>      
         </div>
         <div class="row text-left" style="margin-top: -10px;">
            <?php
               msg("","No Record Found");
               spacer(5);
               }
            ?>
         </div>
      </div>
      <div class="panel-bottom"></div>
   </div>
   <?php spacer(5); ?>
   <div class="mypanel">
      <div class="panel-top">Detail</div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-12">
               <input type="checkbox" class="enabler--" name="chkEnabledsalaryInfo" id="chkEnabledsalaryInfo"  for="empsalaryInfo">
               <label id="enable" for="chkEnabledsalaryInfo">Enable Fields</label>
            </div> 
         </div>
         <?php bar(); ?>
         <div class="row">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Effectivity Date:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="SIEffectivityDate" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Description:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="SIDesc" class="saveFields-- form-input">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Salary Grade:</label>
                  </div>
                  <div class="col-xs-2">
                     <?php $ftable = "SalaryGrade";
                        createSelect($ftable,$ftable."RefId","",100,"Name","Select Salary Grade","");
                     ?>
                  </div>
                  <div class="col-xs-2 label">
                     <label>Job Grade:</label>
                  </div>
                  <div class="col-xs-2">
                     <?php $ftable = "JobGrade";
                        createSelect($ftable,$ftable."RefId","",100,"Name","Select Job Grade","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Old Rate:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="OldRate" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 label">
                     <label>Pay Rate:</label>
                  </div>
                  <div class="col-xs-2">
                     <?php $ftable = "payrate";
                        createSelect($ftable,$ftable."RefId","",100,"Name","Select SIPayrate","");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>New Rate:</label>
                  </div>
                  <div class="col-xs-2 label">
                     <input type="text" name="NewRate" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 label">
                     <label>Adjustment:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="Adjustment" class="saveFields-- form-input">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="panel-bottom"></div>
   </div>
</div>