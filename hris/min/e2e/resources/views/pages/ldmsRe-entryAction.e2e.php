<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IndividualDevtPlan"); ?>"></script>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("RE-ENTRY ACTION PLAN"); ?>
            <div class="container-fluid margin-top">  
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12" style="padding: 10px;">
                        <div class="panel-top">
                           RE-ENTRY ACTION PLAN
                        </div>
                        <div class="panel-mid" style="padding: 15px;">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="row" style="border:1px solid black; padding: 10px;">
                                    <div class="col-xs-12">
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <label>Learner</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <label>Office/Division</label>
                                                <input type="text" class="form-input">
                                             </div>
                                             <div class="col-xs-6">
                                                <label>Date Conducted</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Title of Interventions</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php spacer(40); ?>
                                 <div class="row" style="border:1px solid black; padding: 10px;">
                                    <div class="col-xs-12">
                                       <div class="form-group">
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>REAP Title</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Objectives</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Duration</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Expected Outputs</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Success Indicators</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row" style="padding: 10px;">
                                    <div class="col-xs-12">
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <label>Specific Actions</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <?php spacer(20); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Resources</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <?php spacer(20); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Target Date</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <?php spacer(20); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Status/Remarks</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <?php spacer(20); ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Budgetary Requirements</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>




