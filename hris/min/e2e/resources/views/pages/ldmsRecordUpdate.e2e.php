<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IndividualDevtPlan"); ?>"></script>
      <script language="JavaScript">
         $(document).ready(function () {
            $(".newDataLibrary").hide();
            $("select").css("width","100%");
         });
         
         function selectMe(emprefid) {
            $("[name=\'hEmpRefId\']").val(emprefid);
            $.get("ldmsEmpDetail.e2e.php",
            {
               EmpRefId:emprefid,
               hCompanyID:$("#hCompanyID").val(),
               hBranchID:$("#hBranchID").val(),
               hEmpRefId:$("#hEmpRefId").val(),
               hUserRefId:$("#hUserRefId").val()
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                      if (e instanceof SyntaxError) {
                          alert(e.message);
                      }
                  }
               }
            });
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("RECORD UPDATE"); ?>
            <div class="container-fluid margin-top">  
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-sm-9" style="padding: 10px;">
                        <div class="row" style="padding-left: 5px;">
                           <button class="btn-cls4-sea">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              &nbsp;Save
                           </button>
                        </div>
                        <div class="panel-top margin-top">RECORD UPDATE</div>
                        <div class="panel-mid" style="padding: 15px;">
                           <div class="row">
                              <div class="col-sm-8">
                                 <div class="row" style="padding: 10px;">
                                    <div class="col-sm-12">
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-sm-4">
                                                <label>Name of Official/Employee</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="employeename" readonly>
                                                <input type="hidden" name="hEmpRefId">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4 label">
                                                <label>Office/Division</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="DivisionRefId" readonly>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-3">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                       A. EDUCATION
                                       <br>
                                       (starting with the most recent)
                                    </div>
                                    <div class="panel-body">
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="row margin-top">
                                                <label>Course/Title Degree</label>
                                                <?php
                                                   createSelect("course",
                                                                "sint_CourseRefId",
                                                                "",100,"Name","Select Course","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Date/Finish Graduated</label>
                                                <input type="text" class="form-input date--">
                                             </div>
                                             <div class="row margin-top">
                                                <label>School/Institution</label>
                                                <?php
                                                   createSelect("schools",
                                                                "sint_SchoolsRefId",
                                                                "",100,"Name","Select School","");
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                       B. ELIGIBILITY
                                       <br>
                                       (starting with the most recent)
                                    </div>
                                    <div class="panel-body">
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="row margin-top">
                                                <label>Type of Eligibility Obtained</label>
                                                <?php
                                                   createSelect("CareerService",
                                                                "sint_CareerServiceRefId",
                                                                "",100,"Name","Select Career Service","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Date Obtained</label><br>
                                                <input type="text" class="form-input date--">
                                             </div>
                                             <div class="row margin-top">
                                                <label>Issuing Office</label>
                                                <?php
                                                   createSelect("Office",
                                                                "sint_OfficeRefId",
                                                                "",100,"Name","Select Office","");
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                       C. WORK EXPERIENCE
                                       <br>
                                       (starting with the most recent)
                                    </div>
                                    <div class="panel-body">
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="row margin-top">
                                                <label>Position</label>
                                                <?php
                                                   createSelect("Position",
                                                                "sint_PositionRefId",
                                                                "",100,"Name","Select Position","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Agency/Company</label>
                                                <?php
                                                   createSelect("Agency",
                                                                "sint_AgencyRefId",
                                                                "",100,"Name","Select Agency","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Department</label>
                                                <?php
                                                   createSelect("Department",
                                                                "sint_DepartmentRefId",
                                                                "",100,"Name","Select Department","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Division</label>
                                                <?php
                                                   createSelect("Division",
                                                                "sint_DivisionRefId",
                                                                "",100,"Name","Select Division","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Inclusive Date</label><br>
                                                <input type="text" class="form-input date--">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading text-center">
                                       D. TRAINING
                                       <br>
                                       (starting with the most recent)
                                    </div>
                                    <div class="panel-body">
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="row margin-top">
                                                <label>Program Training Course</label>
                                                <?php
                                                   createSelect("Seminars",
                                                                "sint_SeminarsRefId",
                                                                "",100,"Name","Select Seminars","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Date Attended</label><br>
                                                <input type="text" class="form-input date--">
                                             </div>
                                             <div class="row margin-top">
                                                <label>No. of Hours</label>
                                                <input type="text" class="form-input">
                                             </div>
                                             <div class="row margin-top">
                                                <label>Training Institution/Sponsoring Institution</label>
                                                <?php
                                                   createSelect("Sponsor",
                                                                "sint_SponsorRefId",
                                                                "",100,"Name","Select Sponsor","");
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <label>Competencies Acquired</label>
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                        </div>
                     </div>
                  </div>
               </div>

            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>