<?php
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   require_once "incUtilitiesJS.e2e.php";
?>
<script type="text/javascript">
	$(document).ready(function () {
		$("#checkAll").change(function(){
	      if ($(this).is(':checked')) {
	         $(".showCol--").prop("checked",true);
	      } else {
	         $(".showCol--").prop("checked",false);
	      }
	      $("[name='chkEmpName']").prop("checked",true);
	   });
	});
</script>
<div class="mypanel" id="rptCriteria">
    <div class="panel-mid-litebg">
		<div class="row margin-top">
			<div class="col-xs-2 txt-right">
			   <span class="label">Education Level:</span>
			</div>
			<div class="col-xs-2">
			   <select class="form-input rptCriteria--" name="drpEducLvl" id="drpEducLvl">
				  <option value="0">All</option>
				  <option value="1">1. Elementary</option>
				  <option value="2">2. Secondary</option>
				  <option value="3">3. Vocational</option>
				  <option value="4">4. College Graduate</option>
				  <option value="5">5. Graduate Studies</option>
			   </select>
			</div>
		</div>
		<div class="row margin-top">
			<div class="col-xs-2 txt-right">
				<span class="label">Training Start Date:</span>
			</div>
			<div class="col-xs-4">
			   <span class="label">From:</span>
			   <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtSTrainingFrom" placeholder="Lower Date">
			   <span class="label">To:</span>
			   <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtSTrainingTo" placeholder="Higher Date">
			   <a href="javascript:clearFields('txtSTrainingFrom,txtSTrainingTo');" class="clearflds--">Clear</a>
			</div>
			<div class="col-xs-2 txt-right">
			   <span class="label">Training End Date:</span>
			</div>
			<div class="col-xs-4">
			   <span class="label">From:</span>
			   <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtETrainingFrom" placeholder="Lower Date">
			   <span class="label">To:</span>
			   <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtETrainingTo" placeholder="Higher Date">
			   <a href="javascript:clearFields('txtETrainingFrom,txtETrainingTo');" class="clearflds--">Clear</a>
			</div>
		</div>

        <div class="row margin-top">
            <div class="col-xs-2 txt-right">
               <span class="label">Work Start Date:</span>
            </div>
            <div class="col-xs-4">
               <span class="label">From:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtWorkSFrom" placeholder="Lower Date">
               <span class="label">To:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtWorkSTo" placeholder="Higher Date">
               <a href="javascript:clearFields('txtWorkSFrom,txtWorkSTo');" class="clearflds--">Clear</a>
            </div>
            <div class="col-xs-2 txt-right">
               <span class="label">Work End Date:</span>
            </div>
            <div class="col-xs-4">
               <span class="label">From:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtWorkEFrom" placeholder="Lower Date">
               <span class="label">To:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtWorkETo" placeholder="Higher Date">
               <a href="javascript:clearFields('txtWorkEFrom,txtWorkETo');" class="clearflds--">Clear</a>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-xs-2 txt-right">
               <span class="label">Vol. Work Start Date:</span>
            </div>
            <div class="col-xs-4">
               <span class="label">From:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtVWorkSFrom" placeholder="Lower Date">
               <span class="label">To:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtVWorkSTo" placeholder="Higher Date">
               <a href="javascript:clearFields('txtVWorkSFrom,txtVWorkSTo');" class="clearflds--">Clear</a>
            </div>
            <div class="col-xs-2 txt-right">
               <span class="label">Vol. Work End Date:</span>
            </div>
            <div class="col-xs-4">
               <span class="label">From:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtVWorkEFrom" placeholder="Lower Date">
               <span class="label">To:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtVWorkETo" placeholder="Higher Date">
               <a href="javascript:clearFields('txtVWorkETo,txtVWorkEFrom');" class="clearflds--">Clear</a>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-xs-2 txt-right">
               <span class="label">Eligibility Exam Date:</span>
            </div>
            <div class="col-xs-4">
               <span class="label">From:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtEligiExamFrom" placeholder="Lower Date">
               <span class="label">To:</span>
               <input type="text" class="form-input date-- rptCriteria-- wid30" style="width:30%;" name="txtEligiExamTo" placeholder="Higher Date">
               <a href="javascript:clearFields('txtEligiExamFrom,txtEligiExamTo');" class="clearflds--">Clear</a>
            </div>
        </div>

		<div class="panel-top margin-top">
			Options
		</div>
		<div class="panel-mid-litebg">
			<div class="row margin-top">
				<div class="col-xs-12 txt-center">
					<span class="label">Sort By:</span>
					<select class="form-input rptCriteria--" name="drpSortBy" id="drpSortBy" style="width:150px;">
						<option value="LastName" selected>Last Name</option>
						<option value="FirstName">First Name</option>
						<option value="BirthDate">Date of Birth</option>
						<option value="Height">Height</option>
						<option value="Weight">Weight</option>
						<option value="CivilStatus">Civil Status</option>
					</select>
					<label style="margin-left:10px;margin-right:10px;">|</label>
					<input type="checkbox" name="chkRptSummary" class="showCol--">&nbsp;<span class="label">Show Report Summary</span>
				</div>
			</div>
		</div>
		<div id="column">
			<div class="panel-top">
				Show Column
			</div>
			<div class="panel-mid-litebg padd10">
				<div class="row">
					<div class="col-xs-3">
						<input type="checkbox" id="checkAll"/>&nbsp;<span class="label">Check All</span>
						<?php bar();?>
					</div>
				</div>
				<?php
					$ObjList =
					[
					   [
						  "chkEmpRefId|EmpRefId|checked|Employees Ref. Id",
						  "chkEmpName|EmpName|checked disabled|Employees Name",
						  "chkBirth|Birth|checked|BirthDate",
						  "chkSex|Sex|checked|Sex"
					   ],
					   [
						  "chkCStatus|CStatus|checked|Civil Status",
						  "chkCitizenship|Citizenship||Citizenship",
						  "chkPBirth|PBirth||Birth Place",
						  "chkBlood|Blood||Blood Type"
					   ],
					   [
						  "chkHeight|Height||Height(m)",
						  "chkWeight|Weight||Weight(lbs)",
						  "chkGSIS|GSIS||GSIS No.",
						  "chkPAGIBIG|PAGIBIG||PAGIBIG No."
					   ],
					   [
						  "chkResAddress|ResAddress||Residential Address",
						  "chkResTelNo|ResTelNo||Residential Tel. No.",
						  "chkPermAddress|PermAddress||Permanent Address",
						  "chkPermTelNo|PermTelNo||Permanent Tel. No."
					   ],
					   [
						  "chkEmail|Email||Email Address",
						  "chkMobileNo|MobileNo|checked|Mobile No.",
						  "chkAgencyEmpNo|AgencyEmpNo|checked|Agency Emp. No.",
						  "chkTinNo|TinNo||TIN No."
					   ],
					   [
						  "chkPHILHEALTHNo|PHILHEALTHNo||PHILHEALTH No.",
						  "chkTrainStartDate|TrainStartDate||Training Start Date",
						  "chkTrainEndDate|TrainEndDate||Training End Date",
						  "chkEducLvl|LevelType||Education Level"
					   ],
					   [
						  "chkStrWorkDate|StrWorkDate||Work Start Date",
						  "chkEndWorkDate|StrEndDate||Work End Date",
					   ]
					];
	             	for ($j=0;$j<count($ObjList);$j++) {
					   $RowElement = $ObjList[$j];
					   echo
					   '<div class="row margin-top">';
					   for ($d=0;$d<count($RowElement);$d++) {
						  $obj = explode("|",$RowElement[$d]);

						  if ($obj[0] != "") {
							 echo
							 '<div class="col-xs-3 colopt">
								<input type="checkbox" id="'.$obj[1].'" name="'.$obj[0].'" class="showCol--" '.$obj[2].'>
								<label class="label" for="'.$obj[1].'">'.$obj[3].'</label>
							 </div>';
						  }

					   }
					   echo
					   '</div>'."\n";
					}
				?>
			</div>
		</div>
	</div>
</div>
