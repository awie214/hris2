<?php 
   require_once "incUtilitiesJS.e2e.php";
?>
<script language="JavaScript" src="<?php echo jsCtrl('ctrl_amsFileSetup_Table'); ?>"></script>
<script language="JavaScript">
   $(document).ready(function(){
      $("#LocInsert").click(function () {
         var idx = $(this).attr("idx"); 
         $("#hmode").val("ADD");
         $(".saveFields--").val("");
         $("#tbl_" + idx).slideUp(300);
         $("#TableSet").slideDown(300);
         //$("#TableSet").modal();
         
      });
      $("#btnLocCancelSet, #btnLocCancelItem").click(function () {

         $("#TableItem").modal("hide");
         $("#TableSet").slideUp(300);
         $("#tbl_" + $("[name='tbl']").val()).slideDown(300);
         //$("#TableSet").modal("hide");
      });
      $("#btnLocSaveItem").click(function () {
         $("[name='fn']").val("DBTransac");
         $.ajax({
            url: "trn.e2e.php",
            type: "POST",
            data: new FormData($('form#xForm')[0]),
            /*dataType: 'json',*/
            success : function(responseTxt){
               if (responseTxt.indexOf("Error") < 0) {
                  $("#DataList").html(responseTxt);
                  $("#TableItem").modal("hide");
               } else {
                  console.log(responseTxt);
               }
            },
            error : function (xhr,status,error) {
               console.log(error);
            },
            processData: false,
            contentType: false,
            cache: false
         });
      });
      $("#btnLocSaveSet").click(function () {
         $("[name='fn']").val("DBTransac2");
         if ($("[name='date_EffectivityDate']").val() != "") {
            if (saveProceed() <= 0) {
               $.ajax({
                  url: "trn.e2e.php",
                  type: "POST",
                  data: new FormData($('form#xForm')[0]),
                  success : function(responseTxt){
                     if (responseTxt.indexOf("Error") < 0) {
                        $("#DataList").html(responseTxt);
                        $("#TableSet").modal("hide");
                        afterNewSave();
                     } else {
                        console.log(responseTxt);
                     }
                  },
                  error : function (xhr,status,error) {
                     console.log(error);
                  },
                  processData: false,
                  contentType: false,
                  cache: false
               });
            }
         } else {
            $("[name='date_EffectivityDate']").focus();
            alert("Need Effectivity Date");
         }
      });
      $("#TableSet").hide();
      <?php
         switch ($menuID) {
            case "mTable1":
               echo '$("#hTable").val("slvlearnedmonthly");';
            break;
            case "mTable2":
               echo '$("#hTable").val("slvlearneddaily");';
            break;
            case "mTable3":
               echo '$("#hTable").val("leavecreditsearnedwopay");';
            break;
            case "mTable4":
               echo '$("#hTable").val("workinghrsconversion");';
            break;
         }
      ?>
   });
   function loadTable(tblNo,effDate) {
      $.get("trn.e2e.php",
      {
         fn:"LoadTable",
         tblNo:tblNo,
         effDate:effDate
      },
      function(data,status) {
         if (status == "success") {
            try {
               eval(data);
            } catch (e) {
                if (e instanceof SyntaxError) {
                    alert(e.message);
                }
            }
         }
      });
   }

</script>
<style>
   #DataList {
      overflow:auto;
      max-height:400px;
      margin:auto;
      text-align:center;
   }
</style>

<?php
   function dobtn() {
      echo
      '<hr>
      <div class="row">
         <div class="col-xs-12 txt-center">';
               createButton("Save","btnLocSaveItem","btn-cls4-sea","fa-floppy-o","");
               createButton("Cancel","btnLocCancelItem","btn-cls4-red","fa-undo","");
         echo
         '</div>
      </div>';
   }
   function insertIcon($idx) {
      echo
      '<a href="javascript:void(0);">
         <i class="fa fa-plus-square" aria-hidden="true" id="LocInsert" idx="'.$idx.'" title="Insert New Table Set" style="color:white;"></i>
      </a>';
   }

?>
<input type="hidden" name="fn" value="DBTransac">
<div id="EntryScrn">
   <?php
      spacer(5);
      $today = date("Y-m-d",time());
      switch ($menuID) {
         case "mTable1":
   ?>
            <div class="mypanel" id="tbl_1">
               <div class="panel-top">
                  <?php insertIcon(1);?>&nbsp;Table I
               </div>
               <div class="panel-mid txt-center">
                  <h5>VACATION AND SICK LEAVE CREDITS EARNED ON A MONTHLY BASIS</h5>
                  <div id="DataList" class="txt-center">
                     <?php
                        $attr = array (
                                    "ColHdr"=>["EFF.DATE", "NO. OF MONTHS","VACATION LEAVE EARNED","SICK LEAVE EARNED"],
                                    "ColFld"=>["EffectivityDate","NoOfMonths","VLEarned","SLEarned"],
                                    "clause"=>"where EffectivityDate <= '$today' ORDER BY EffectivityDate DESC, NoOfMonths LIMIT 12",
                                    "dbtable"=>"slvlearnedmonthly"
                                 );
                        updateListTable($attr);
                     ?>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade modalFieldEntry--" id="TableItem" role="dialog">
               <div class="modal-dialog">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Updating Record</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("No. of Months:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","sint_NoOfMonths","NumMonths","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Vacation Leave Earned:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","deci_VLEarned","VLEarned","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Sick Leave Earned:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","deci_SLEarned","SLEarned","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Remarks:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                              </div>
                           </div>
                           <?php dobtn(); ?>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <!--<div class="modal fade modalFieldEntry--" id="TableSet" role="dialog">
               <div class="modal-dialog">-->

                  <div class="mypanel" id="TableSet" style="height:100%;">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New Set Of Table I</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                        <?php
                           $sql = "Select distinct EffectivityDate from slvlearnedmonthly order by EffectivityDate";
                           $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                           if (mysqli_num_rows($result) > 0) {
                              echo '<div class="row">';
                              echo '<div class="col-xs-5">';
                              echo '<label>Select Here for History:</label><br>';
                              echo '<select onchange="loadTable(1,this.value);" class="form-input">
                              <option value=""></option>';
                              while ($row = mysqli_fetch_array($result)) {
                                 echo
                                 '<option value="'.$row["EffectivityDate"].'">'.$row["EffectivityDate"].'</option>';
                              }
                              echo '</select>
                                 </div>';
                              echo '</div><br>';
                           }
                           echo
                           '<input type="text" name="date_EffectivityDate" class="form-input date--" placeholder="Effectivity Date"><br>';
                           echo '<div class="row">';
                           echo '<div class="col-xs-3">Number Of Months</div>';
                           echo '<div class="col-xs-3">Vacation Leave Earned</div>';
                           echo '<div class="col-xs-3">Sick Leave Earned</div>';
                           echo '</div>';
                              for ($j=1;$j<=12;$j++) {
                                 echo '<div class="row margin-top">';
                                 echo '<div class="col-xs-3">
                                 <input type="text" name="sint_NoOfMonths_'.$j.'" value="'.$j.'" class="form-input number--" readonly>
                                 </div>';
                                 echo '<div class="col-xs-3">
                                 <input type="text" name="deci_VLEarned_'.$j.'" class="form-input number--"></div>';
                                 echo '<div class="col-xs-3">
                                 <input type="text" name="deci_SLEarned_'.$j.'" class="form-input number--"></div>';
                                 echo '</div>';
                              }
                        ?>
                        <hr>
                        <div class="row">
                           <div class="col-xs-12 txt-center">
                               <?php
                                 createButton("Save","btnLocSaveSet","btn-cls4-sea","fa-floppy-o","");
                                 createButton("Cancel","btnLocCancelSet","btn-cls4-red","fa-undo","");
                                 createButton("Clear","btnClearSet","btn-cls4-lemon","fa-minus","");
                               ?>

                           </div>
                        </div>
                        <!-- <script type="text/javascript" src="<?php //echo path("datepicker/js/bootstrap-datepicker.js");?>"></script>
                        <script type="text/javascript" src="<?php //echo path("js/js6_jquery_utilities.js");?>"></script>-->
                     </div>
                  </div>
               <!--    
               </div>
            </div> -->
            <input type="hidden" name="tbl" value="1">

   <?php
         break;
         case "mTable2":
   ?>
            <div class="mypanel" id="tbl_2">
               <div class="panel-top">
                  <?php insertIcon(2);?>&nbsp;Table II
               </div>
               <div class="panel-mid txt-center">
                  <h5>VACATION AND SICK LEAVE CREDITS EARNED ON A DAILY BASIS</h5>
                  <div id="DataList">
                     <?php
                        $today = date("Y-m-d",time());
                        $attr = array (
                                    "ColHdr"=>["EFF. DATE","NO. OF DAYS","VACATION LEAVE EARNED","SICK LEAVE EARNED"],
                                    "ColFld"=>["EffectivityDate","NoOfDays","VLEarned","SLEarned"],
                                    "clause"=>"where EffectivityDate <= '$today' ORDER BY EffectivityDate DESC, NoOfDays LIMIT 31",
                                    "dbtable"=>"slvlearneddaily"
                                 );
                        updateListTable($attr);
                     ?>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade modalFieldEntry--" id="TableItem" role="dialog">
               <div class="modal-dialog">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("No. of Days:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","sint_NoOfDays","NumDays","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Vacation Leave Earned:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","deci_VLEarned","VLEarned","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Sick Leave Earned:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","deci_SLEarned","SLEarned","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Remarks:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                              </div>
                           </div>
                           <input type="hidden" name="fn" value="DBTransac">
                           <input type="hidden" name="tbl" value="2">
                           <?php dobtn(); ?>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <!-- 
            <div class="modal fade modalFieldEntry--" id="TableSet" role="dialog">
               <div class="modal-dialog" style="width:50%;"> -->

                  <div class="mypanel" id="TableSet" style="height:100%;width:75%">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New Set Of Table II</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid" style="max-height:500px;overflow:auto;">
                        <?php
                           $sql = "Select distinct EffectivityDate from slvlearneddaily order by EffectivityDate";
                           $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                           if (mysqli_num_rows($result) > 0) {
                              echo '<div class="row">';
                              echo '<div class="col-xs-5">';
                              echo '<label>Select Here for History:</label><br>';
                              echo '<select onchange="loadTable(2,this.value);" class="form-input">
                              <option value=""></option>';
                              while ($row = mysqli_fetch_array($result)) {
                                 echo
                                 '<option value="'.$row["EffectivityDate"].'">'.$row["EffectivityDate"].'</option>';
                              }
                              echo '</select>
                                 </div>';
                              echo '</div><br>';
                           }

                           echo
                           '<input type="text" name="date_EffectivityDate" class="form-input date--" placeholder="Effectivity Date"><br>';
                           echo '<div class="row">';
                           echo '<div class="col-xs-4">Number Of Days</div>';
                           echo '<div class="col-xs-4">Vacation Leave Earned</div>';
                           echo '<div class="col-xs-4">Sick Leave Earned</div>';
                           echo '</div>';
                              for ($j=1;$j<=31;$j++) {
                                 echo '<div class="row margin-top">';
                                 echo '<div class="col-xs-4"><input type="text" name="sint_NoOfDays_'.$j.'" value="'.$j.'" class="form-input number--" readonly></div>';
                                 echo '<div class="col-xs-4"><input type="text" name="deci_VLEarned_'.$j.'" class="form-input number-- x-"></div>';
                                 echo '<div class="col-xs-4"><input type="text" name="deci_SLEarned_'.$j.'" class="form-input number-- x-"></div>';
                                 echo '</div>';
                              }
                        ?>
                        <hr>
                        <div class="row">
                           <div class="col-xs-12 txt-center">
                               <?php
                                 createButton("Save","btnLocSaveSet","btn-cls4-sea","fa-floppy-o","");
                                 createButton("Cancel","btnLocCancelSet","btn-cls4-red","fa-undo","");
                                 createButton("Clear","btnClearSet","btn-cls4-lemon","fa-minus","");
                               ?>

                           </div>
                        </div>
                     </div>
                  </div>
               <!--    
               </div>
            </div>-->
            <input type="hidden" name="tbl" value="2">
   <?php
         break;
         case "mTable3":
         $table = "leavecreditsearnedwopay";
   ?>
            <div class="mypanel" id="tbl_3">
               <div class="panel-top">
                  <?php insertIcon(3);?>&nbsp;Table III
               </div>
               <div class="panel-mid txt-center">
                  <h5>
                     LEAVE CREDITS EARNED IN A MONTH BY OFFICIAL / EMPLOYEE<br>
                     WITHOUT ANY VACATION LEAVE CREDIT LEFT
                  </h5>
                  <div id="DataList">
                     <?php
                        $attr = array (
                                    "ColHdr"=>["EFF. DATE","NO. OF DAYS PRESENT","NO. OF DAYS ON LEAVE W/O PAY","LEAVE CREDITS EARNED"],
                                    "ColFld"=>["EffectivityDate","NoOfDaysPresent","NoOfDaysLeaveWOP","LeaveCreditsEarned"],
                                    "clause"=>"where EffectivityDate <= '$today' ORDER BY EffectivityDate DESC, NoOfDaysPresent DESC LIMIT 61",
                                    "dbtable"=>"leavecreditsearnedwopay"
                                 );
                        updateListTable($attr);
                     ?>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade modalFieldEntry--" id="TableItem" role="dialog">
               <div class="modal-dialog">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                        <div class="row margin-top">
                           <div class="col-xs-4 txt-right">
                              <?php label("No. Of Days Present:",""); ?>
                           </div>
                           <div class="col-xs-6">
                              <?php createForm("text","deci_NoOfDaysPresent","NoOfDaysPresent","","","saveFields-- number--"); ?>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4 txt-right">
                              <?php label("No. Of Days Leave W/O Pay:",""); ?>
                           </div>
                           <div class="col-xs-6">
                              <?php createForm("text","deci_NoOfDaysLeaveWOP","NoOfDaysLeaveWOP","","';","saveFields-- number--"); ?>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4 txt-right">
                              <?php label("Leave Credits Earned:",""); ?>
                           </div>
                           <div class="col-xs-6">
                              <?php createForm("text","deci_LeaveCreditsEarned","LeaveCreditsEarned","","","saveFields-- number--"); ?>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4 txt-right">
                              <?php label("Remarks:",""); ?>
                           </div>
                           <div class="col-xs-6">
                              <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                           </div>
                        </div>
                        <input type="hidden" name="tbl" value="3">
                        <?php dobtn(); ?>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <!-- 
            <div class="modal fade modalFieldEntry--" id="TableSet" role="dialog">
               <div class="modal-dialog" style="width:50%">-->

                  <div class="mypanel" id="TableSet" style="height:100%;width:75%">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New Set Of Table III</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid" style="max-height:500px;overflow:auto;">
                        <?php
                           $sql = "Select distinct EffectivityDate from `$table` order by EffectivityDate";
                           $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                           if (mysqli_num_rows($result) > 0) {
                              echo '<div class="row">';
                              echo '<div class="col-xs-5">';
                              echo '<label>Select Here for History:</label><br>';
                              echo '<select onchange="loadTable(2,this.value);" class="form-input">
                              <option value=""></option>';
                              while ($row = mysqli_fetch_array($result)) {
                                 echo
                                 '<option value="'.$row["EffectivityDate"].'">'.$row["EffectivityDate"].'</option>';
                              }
                              echo '</select>
                                 </div>';
                              echo '</div><br>';
                           }
                           echo
                           '<input type="text" name="date_EffectivityDate" class="date-- form-input" placeholder="Effectivity Date"><br>';
                           echo '<div class="row">';
                           echo '<div class="col-xs-4">NO. OF DAYS PRESENT</div>';
                           echo '<div class="col-xs-4">NO. OF DAYS ON LEAVE WITHOUT PAY</div>';
                           echo '<div class="col-xs-4">LEAVE CREDITS EARNED</div>';
                           echo '</div>';
                              $idx = 30;
                              $idx1 = 0;
                              $idx2 = 0;
                              $LVCRED = ["1.250","1.229","1.208","1.188","1.167","1.146","1.125","1.104","1.083","1.063",
                                         "1.042","1.021","1.000","0.979","0.958","0.938","0.917","0.896","0.875","0.854",
                                         "0.833","0.813","0.792","0.771","0.750","0.729","0.708","0.687","0.667","0.646",
                                         "0.625","0.604","0.583","0.562","0.542","0.521","0.500","0.479","0.458","0.437",
                                         "0.417","0.396","0.375","0.354","0.333","0.312","0.292","0.271","0.250","0.229",
                                         "0.208","0.187","0.167","0.146","0.125","0.104","0.083","0.062","0.042","0.021",
                                         "0.000"];
                              for ($j=1;$j<=61;$j++) {
                                 echo '<div class="row margin-top" id="parentDivHolder">';
                                 echo '<div class="col-xs-4">
                                 <input type="text" name="sint_NoOfDaysPresent_'.$j.'" value="'.number_format($idx,2).'" class="form-input number--" readonly>
                                 </div>';
                                 echo '<div class="col-xs-4">
                                 <input type="text" name="deci_NoOfDaysLeaveWOP_'.$j.'" value="'.number_format($idx1,2).'" class="form-input number-- x-">
                                 </div>';
                                 echo '<div class="col-xs-4">
                                 <input type="text" name="deci_LeaveCreditsEarned_'.$j.'" value="'.$LVCRED[$j - 1].'" class="form-input number-- x-">
                                 </div>';
                                 echo '</div>';
                                 $idx = $idx - .5;
                                 $idx1 = $idx1 + .5;
                              }
                        ?>
                        <hr>
                        <div class="row">
                           <div class="col-xs-12 txt-center">
                               <?php
                                 createButton("Save","btnLocSaveSet","btn-cls4-sea","fa-floppy-o","");
                                 createButton("Cancel","btnLocCancelSet","btn-cls4-red","fa-undo","");
                                 createButton("Clear","btnClearSet","btn-cls4-lemon","fa-minus","");
                               ?>

                           </div>
                        </div>
                     </div>
                  </div>
               <!--    
               </div>
            </div>-->
            <input type="hidden" name="tbl" value="3">

   <?php
         break;
         case "mTable4":
            $table = "workinghrsconversion";
   ?>

            <div class="mypanel" id="tbl_4">
               <div class="panel-top">
                  <?php insertIcon(4);?>&nbsp;Table IV
               </div>
               <div class="panel-mid txt-center">
                  <div>
                     <h4>CONVERSION OF WORKING HOURS/MINUTES INTO<br>
                        FRACTIONS OF A DAY
                     </h4>
                     <div id="DataList">
                        <?php
                           $attr = array (
                              "ColHdr"=>["EFF. DATE","NO. OF","EQUIVALENT"],
                              "ColFld"=>["EffectivityDate", "NoOf", "EquivalentDay"],
                              "clause"=>"where EffectivityDate <= '$today' ORDER BY EffectivityDate DESC, NoOf LIMIT 60",
                              "dbtable"=>"workinghrsconversion"
                           );
                           updateListTable($attr);
                        ?>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade modalFieldEntry--" id="TableItem" role="dialog">
               <div class="modal-dialog">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                           <!--<div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php //label("Type:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <select class="form-input saveFields--" name="char_Type">
                                    <option value=""></option>
                                    <option value="hrs">Hours</option>
                                    <option value="min">Minutes</option>
                                 <select>
                              </div>
                           </div>-->
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("No. Of:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","sint_NoOf","NoOf","","';","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Equivalent Day:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <?php createForm("text","deci_EquivalentDay","EquivalentDay","","","saveFields-- number--"); ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 txt-right">
                                 <?php label("Remarks:",""); ?>
                              </div>
                              <div class="col-xs-6">
                                 <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                              </div>
                           </div>
                           <input type="hidden" name="fn" value="DBTransac">
                           <input type="hidden" name="tbl" value="4">
                           <?php dobtn(); ?>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <!--
            <div class="modal fade modalFieldEntry--" id="TableSet" role="dialog">
               <div class="modal-dialog" style="width:50%">-->
                  <div class="mypanel" id="TableSet" style="height:100%;width:75%">
                     <div class="panel-top bgSea">
                        <span id="modalTitle" style="font-size:11pt;">Inserting New Set Of Table IV</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid" style="max-height:500px;overflow:auto;">
                        <?php
                           $sql = "Select distinct EffectivityDate from `$table` order by EffectivityDate";
                           $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                           if (mysqli_num_rows($result) > 0) {
                              echo '<div class="row">';
                              echo '<div class="col-xs-5">';
                              echo '<label>Select Here for History:</label><br>';
                              echo '<select onchange="loadTable(2,this.value);" class="form-input">
                              <option value=""></option>';
                              while ($row = mysqli_fetch_array($result)) {
                                 echo
                                 '<option value="'.$row["EffectivityDate"].'">'.$row["EffectivityDate"].'</option>';
                              }
                              echo '</select>
                                 </div>';
                              echo '</div><br>';
                           }
                           echo
                           '<input type="text" name="date_EffectivityDate" class="date-- form-input" placeholder="Effectivity Date"><br>';
                           echo '<div class="row">';
                           echo '<div class="col-xs-4">MINUTES</div>';
                           echo '<div class="col-xs-4">EQUIVALENT DAY</div>';
                           echo '</div>';
                              $idx = 30;
                              $idx1 = 0;
                              $idx2 = 0;
                              $EQVDAY = [".002",".004",".006",".008",".010",".012",".015",".017",".019",".021",
                                         ".023",".025",".027",".029",".031",".033",".035",".037",".040",".042",
                                         ".044",".046",".048",".050",".052",".054",".056",".058",".060",".062",
                                         ".065",".067",".069",".071",".073",".075",".077",".079",".081",".083",
                                         ".085",".087",".090",".092",".094",".096",".098",".100",".102",".104",
                                         ".106",".108",".110",".112",".115",".117",".119",".121",".123",".125"];
                              for ($j=1;$j<=60;$j++) {
                                 echo '<div class="row margin-top" id="parentDivHolder">';
                                 echo '<div class="col-xs-4">
                                 <input type="text" name="sint_NoOf_'.$j.'" value="'.$j.'" class="form-input number--" readonly>
                                 </div>';
                                 echo '<div class="col-xs-4">
                                 <input type="text" name="deci_EquivalentDay_'.$j.'" value="'.$EQVDAY[$j - 1].'" class="form-input number-- x-">
                                 </div>';
                                 echo '</div>';
                              }
                        ?>
                        <hr>
                        <div class="row">
                           <div class="col-xs-12 txt-center">
                               <?php
                                 createButton("Save","btnLocSaveSet","btn-cls4-sea","fa-floppy-o","");
                                 createButton("Cancel","btnLocCancelSet","btn-cls4-red","fa-undo","");
                                 createButton("Clear","btnClearSet","btn-cls4-lemon","fa-minus","");
                               ?>

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <input type="hidden" name="tbl" value="4">

   <?php
         break;
      }
   ?>
   <div class="margin-top alert alert-warning alert--">
      <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;
      <span id="errMsg"></span>
   </div>
</div>