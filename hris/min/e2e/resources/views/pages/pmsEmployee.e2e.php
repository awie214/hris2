<?php
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   $sys = new SysFunctions();
   $tabTitle = ["Employment Summary",
                "Salary Info",
                "Benefits/Allowance",
                "Loans Info",
                "Deduction Info",
                "Bonus Info"
            ];
   $tabFile = ["inc_pms_employmentSummary",
               "inc_pms_empsalaryInfo",
               "inc_pms_empbenefits",
               "inc_pms_emploans",
               "inc_pms_empdeductions",
               "inc_pms_empbonus"
            ];
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_pmsEmployee"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("EMPLOYEE FILE"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div class="row" id="divList">
                              <div class="col-sm-3">
                                    <?php employeeSelector(); ?>
                              </div>
                              <div class="col-sm-9">
                                    <?php $templ->doSelectedEmployee(); ?>
                                    <div class="row">
                                       <button type="button" id="btnUPDATE" name="btnUPDATE" class="btn-cls4-sea" >UPDATE</button>
                                    </div>
                                    <div class="row" style="margin-top:5px;width:100%;">
                                       <input type="hidden" name="hTabIdx" value="1">
                                       <div class="btn-group btn-group-sm">
                                       <?php
                                             $idx = 0;
                                             $active = "";
                                             for ($j=0;$j<count($tabTitle);$j++) {
                                             $idx = $j + 1;
                                             /*if ($idx == 1) $active = "elActive";
                                                         else $active = "";*/
                                             echo
                                             '<button type="button" name="btnTab_'.$idx.'" class="btn btn-default '.$active.'">'.$tabTitle[$j].'</button>';
                                             }
                                       ?>
                                       </div>
                                       <div class="row">
                                             <div class="col-xs-12">
                                                <?php
                                                   spacer(5);
                                                   $idx = 0;
                                                   for ($j=0;$j<count($tabTitle);$j++) {
                                                         $idx = $j + 1;
                                                         
                                                         fTabs($idx,$tabTitle[$j],$tabFile[$j]);
                                                   }
                                                ?>
                                             </div>
                                       </div>
                                    </div>
                              </div>
                        </div>   
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("fn","SavePMSEmployee","");
               doHidden("json","pmsEmployee","");
            ?>
         </div>
      </form>
   </body>
</html>



