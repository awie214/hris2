<?php
   error_reporting(E_ALL);
   ini_set('display_errors', 1);
   session_start();
   include_once "conn.e2e.php";
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';
   $trn = new Transaction();

   $moduleContent = file_get_contents(json.getvalue("json").'.json');
   $module        = json_decode($moduleContent, true); 
   
   $CompanyID = getvalue("hCompanyID");
   $BranchID = getvalue("hBranchID");
   $UserRefId = getvalue("hUserRefId");
   $WHERE = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
   function fngetEmployeesPMSRecord() {
      $conn = $GLOBALS["conn"];
      $module = $GLOBALS["module"];
      $empRefid = getvalue("emprefid");
      $tab = $module["Module.PMS.Employee.EmploymentSummary"]["dbField"];
      $dbTable = $module["Module.PMS.Employee.EmploymentSummary"]["dbTable"]; 
      $rs_pms_Employees = FindFirst("pms_employees",$GLOBALS["WHERE"]." AND EmployeesRefId = ".$empRefid,"*");
      if (!$rs_pms_Employees) {
         echo '$.notify("No Record Found","warn");';
         foreach ($tab as $key => $value) {
            objSetValue($value[1],CValue($value[2]));
         }
      } else {
         if ($tab != "") {
            foreach ($tab as $key => $value) {
               $fieldname = $value[0];
               if (isset($rs_pms_Employees[$fieldname])) {
                  $fieldvalue = $rs_pms_Employees[$fieldname];
                  objSetValue($value[1],CValue($fieldvalue));
               }
            }
            echo "tabClick(1);";   
         }
         
      }
   }

   function fnSavePMSEmployee() {
	   $empRefid = $_POST["hRefIdSelected"];
      if ($empRefid > 0) {
         saveEmploymentSummary($empRefid);
      }
   }
   
   function saveEmploymentSummary($empRefid) {
      $conn = $GLOBALS["conn"];
      $module = $GLOBALS["module"];
      $table = "pms_Employees";
      $dbTable = $module["Module.PMS.Employee.EmploymentSummary"]["dbTable"];
      $dbField = $module["Module.PMS.Employee.EmploymentSummary"]["dbField"];
      if ($empRefid > 0) {
         $pmsEmployees = FindFirst($table,$GLOBALS["WHERE"]." AND EmployeesRefId = ".$empRefid,"RefId");
         $fv = parseFieldnValue($dbField,$_POST);
         if (!$pmsEmployees) {
            $Fields  = $fv["Fields"]."`EmployeesRefId`,";
            $Values  = $fv["Values"]."'$EmpRefId',";
            $LastIdInserted = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
            if (is_numeric($LastIdInserted)) {
               /*savePMSEmployeeDetail($empRefid,$LastIdInserted,"SalaryInfo","chkEnabledsalaryInfo");
               savePMSEmployeeDetail($empRefid,$LastIdInserted,"BenefitsInfo","chkBenefitsInfo");
               savePMSEmployeeDetail($empRefid,$LastIdInserted,"LoansInfo","chkEnabledloans");  
               savePMSEmployeeDetail($empRefid,$LastIdInserted,"DeductionInfo","chkDeduction");
               savePMSEmployeeDetail($empRefid,$LastIdInserted,"BonusInfo","chkEnabledbonus");*/
               echo "New Record [{$LastIdInserted}] Successfully Inserted";
            } else {
               echo "Error Saving... Transaction Aborted !!! \nPlease Re-Try";
            }
         } else {
            $Fldnval = $fv["FieldsValues"];
            echo $Fldnval;
            $result = f_SaveRecord("EDITSAVE",$table,$Fldnval,$pmsEmployees);
            if ($result == "") {
               /*savePMSEmployeeDetail($empRefid,$pmsEmployees,"SalaryInfo","chkEnabledsalaryInfo","TabName");
               savePMSEmployeeDetail($empRefid,$pmsEmployees,"BenefitsInfo","chkBenefitsInfo","TabName");
               savePMSEmployeeDetail($empRefid,$pmsEmployees,"LoansInfo","chkEnabledloans","TabName");  
               savePMSEmployeeDetail($empRefid,$pmsEmployees,"DeductionInfo","chkDeduction","TabName");
               savePMSEmployeeDetail($empRefid,$pmsEmployees,"BonusInfo","chkEnabledbonus","TabName");*/
               echo "Record [{$empRefid}] Successfully Updated";
            }
            else {
               echo $result;
            }
         } 
      } else {
         echo "No Record Found ... Employees [{$empRefid}]";
      }
   }
   function savePMSEmployeeDetail($empRefid,$lastrefid,$elem2,$chkbox,$elem1) {
      if (isset($_POST[$chkbox])) {
         if ($_POST[$chkbox] == "true" || $_POST[$chkbox] == "on" || $_POST[$chkbox] == 1) {
            $conn = $GLOBALS["conn"];
            $module = $GLOBALS["module"];
            $tab = $module[$elem1][$elem2]["dbField"];
            $dbTable = $module[$elem1][$elem2]["dbTable"];
            if ($empRefid > 0) {
               $checkRecord = FindFirst($dbTable,$GLOBALS["WHERE"]." AND pms_EmployeesRefId = ".$lastrefid,"RefId");
               if (!$checkRecord) {
                  $Fields = "`EmployeesRefId`, `pms_EmployeesRefId`, ";
                  $Values = "'".$empRefid."', '$lastrefid', ";
                  $fv = set_Field_Value("NEWSAVE",$tab,$_POST,$conn,$dbTable);
                  $Fields .= explode("║",$fv)[0];
                  $Values .= explode("║",$fv)[1];
                  /*
                  echo $Fields."\n";
                  echo $Values."\n";
                  */
                  $LastIdInserted = f_SaveRecord("NEWSAVE",$dbTable,$Fields,$Values);
                  if (!is_numeric($LastIdInserted)) {
                     echo "Error Saving... Transaction Aborted !!! \nPlease Re-Try";
                  }
               } else {
                  $fv = set_Field_Value("EDITSAVE",$tab,$_POST,$conn,$dbTable);
                  //echo $fv;
                  $result = f_SaveRecord("EDITSAVE",$dbTable,$fv,$checkRecord);
                  //echo $result;
                  if ($result != "") {
                     echo $result;
                  }
               } 
            } else {
               echo "No Record Found ... [{$empRefid}]";
               return false;
            }   
         }   
      }
   }
   
   /*DONT MODIFY HERE*/
   $funcname = "fn".getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }
?>