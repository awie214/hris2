<?php
   $_SESSION["sysData"] = "DataLibrary";
   $fContent = file_get_contents(json."file.json");
   $files = json_decode($fContent, true);
   $File = "FM_". getvalue("f");
   $tbl = getvalue("f");
   switch ($File){
      case "FM_Agency":
         $model = "modelAgency";
      break;
      case "FM_Branch":
         $model = "modelBranch";
      break;
      case "FM_Department":
         $model = "modelDept";
      break;
      case "FM_Office":
         $model = "modelOffice";
      break;
      case "FM_Designation":
         $model = "modelDesignation";
      break;
      case "FM_Division":
         $model = "modelDivision";
      break;
      case "FM_Locations":
         $model = "modelLocations";
      break;
      case "FM_PositionClassification":
         $model = "modelPosClass";
      break;
      case "FM_PositionLevel":
         $model = "modelPosLevel";
      break;
      case "FM_Position":
         $model = "modelPosition";
      break;
      case "FM_PositionItem":
         $model = "modelPosItem";
      break;
      case "FM_Sections":
         $model = "modelSections";
      break;
      case "FM_Units":
         $model = "modelUnits";
      break;
      case "FM_PayRate":
         $model = "modelPayRate";
      break;
      case "FM_City":
         $model = "modelCity";
      break;
      case "FM_Province":
         $model = "modelProvince";
      break;
      case "FM_ApptStatus":
         $model = "modelApptStatus";
      break;
      case "FM_BloodType":
         $model = "modelBloodType";
      break;
      case "FM_Citizenship":
         $model = "modelCitizenship";
      break;
      case "FM_Organization":
         $model = "modelOrg";
      break;
      case "FM_CourseCategory":
         $model = "modelCourseCat";
      break;
      case "FM_Course":
         $model = "modelCourse";
      break;
      case "FM_Schools":
         $model = "modelSchools";
      break;
      case "FM_CareerService":
         $model = "modelCareerService";
      break;
      case "FM_SeminarClass":
         $model = "modelSeminarClassifications";
      break;
      case "FM_SeminarPlace":
         $model = "modelSeminarPlace";
      break;
      case "FM_Sponsor":
         $model = "modelSeminarSponsor";
      break;
      case "FM_SeminarType":
         $model = "modelSeminarType";
      break;
      case "FM_SalaryGrade":
         $model = "modelSalaryGrade";
      break;
      case "FM_StepIncrement":
         $model = "modelStepIncrement";
      break;
      case "FM_CaseNature":
         $model = "modelCaseNature";
      break;
      case "FM_Seminars":
         $model = "modelSeminars";
      break;
      case "FM_SysGroup":
         $model = "modelSysGroup";
      break;
      case "FM_SysModules":
         $model = "modelModules";
      break;
      case "FM_EmpStatus":
         $model = "modelEmpStatus";
      break;
      case "FM_awards":
         $model = "modelAwards";
      break;
      case "FM_Occupations":
         $model = "modelOccupations";
      break;
      case "FM_Forms":
         $model = "modelForms";
      break;
      default:
         $model = "";
      break;
   }
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_FMCaller"); ?>"></script>
   </head>
   <body>
      <form name="xForm">
         <input type="hidden" name="hFMTable" id="hFMTable" value="<?php echo $tbl; ?>">
         <input type="hidden" name="isFileManager" id="isFileManager" value="YES">
         <input type="hidden" name="isPOP" id="isPOP" value="yes">
         <div class="container-fluid">
            <?php spacer(5); ?>
            <div class="mypanel">
                           <div class="panel-top">INSERT NEW <?php echo $tbl; ?></div>
                           <div class="panel-mid-litebg" id="FM_EntryScreen">
               <?php
                  if ($model != "")
                  {
                     include $model.".e2e.php";
               ?>
                           </div>
                           <div class="panel-bottom">
                              <div class="row margin-top">
                                 <div class="col-xs-12 txt-center">
                                    <span id="spSAVECANCEL">
                                       <button type="button" class="btn-cls4-sea"
                                               name="btnFM_SAVE" id="btnFM_SAVE">
                                          <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                          &nbsp;Save
                                       </button>
                                       <button type="button" class="btn-cls4-red" onclick="self.close();">
                                       <i class="fa fa-times" aria-hidden="true"></i>
                                       &nbsp;Cancel</button>
                                    </span>
                                 </div>
                                 <input type="hidden" name="dropobj" value="<?php echo getvalue("objid"); ?>">
                              </div>
                           </div>
               <?php } else {
                 echo "<span>Model Not Available</span>";
               } ?>

            </div>
         <?php
            footer();
            include 'varHidden.e2e.php';
         ?>
         </div>
      </form>
   </body>
</html>
