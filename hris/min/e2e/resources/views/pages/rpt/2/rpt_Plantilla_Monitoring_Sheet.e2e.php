<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) { echo "DBG >> ".$whereClause; }
?>
<!DOCTYPE html>
<html>
   	<head>
      	<?php include_once $files["inc"]["pageHEAD"]; ?>
      	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   	</head>
   	<body>
      	<div class="container-fluid rptBody">
		<table>
			<thead>
				<tr>
                  <th colspan="19" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <?php spacer(30); ?>
                  </th>
               </tr> 
				<tr class="colHEADER">
					<th style="width: 20%;">FULL NAME</th>
					<th>POSITION</th>
					<th>DIVISION</th>
					<th>OFFICE</th>
					<th>PLANTILLA ITEM NO.</th>
					<th>JOB<br>GRADE</th>
					<th>MONTHLY<br>SALARY</th>
					<th>DATE OF<br>ASSUMPTION TO<br>DUTY</th>
					<th>NATURE OF <br> APPOINTMENT</th>
					<th>STATUS OF <br> APPOINTMENT</th>
					<th>PREVIOUS <br> EMPLOYER</th>
					<th>EMAIL ADDRESS</th>
					<th>RESIDENTAL <br> ADDRESS</th>
					<th>MOBILE <br> NUMBER</th>
					<th>BIRTHDATE</th>
					<th>REMARKS</th>
					<th>HIRING <br> STATUS</th>
					<th>Perm/Co-<br>Term</th>
					<th>EMPLOYEE<br>ORIENTATION</th>
				</tr>
			</thead>
			<tbody>
			<?php 
                while ($row = mysqli_fetch_assoc($rsEmployees)) {
                        $EmployeesRefId = $row["RefId"];
                        $CompanyRefId   = $row["CompanyRefId"];
                        $BranchRefId    = $row["BranchRefId"];
                        $where  = "WHERE CompanyRefId = $CompanyRefId";
                        $where .= " AND BranchRefId = $BranchRefId";
                        $EmailAdd = $row["EmailAdd"];
                        $MobileNo = $row["MobileNo"];
                        $BirthDate = $row["BirthDate"];
                        $ResiAddCity = getRecord("City",$row["ResiAddCityRefId"],"Name");
                        
                        $where .= " AND EmployeesRefId = $EmployeesRefId";
                     	$empinfo_row = FindFirst("empinformation",$where,"*");
                        if ($empinfo_row) {
                        	$Position  = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                        	$Division  = getRecord("Division",$empinfo_row["DivisionRefId"],"Name"); 
                        	$Office    = getRecord("Office",$empinfo_row["OfficeRefId"],"Name"); 
                        	$JobGrade  = getRecord("JobGrade",$empinfo_row["JobGradeRefId"],"Name"); 
                        	$ApptStatus  = getRecord("ApptStatus",$empinfo_row["ApptStatusRefId"],"Name");
                        	$HiredDate = $empinfo_row["HiredDate"];
                        } else {
                        	$Position  = "";
                        	$Division  = "";
                        	$Office    = "";
                        	$JobGrade  = "";
                        	$ApptStatus = "";			
                        }

               ?>
				<tr>
					<td><?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]; ?></td>
					<td><?php echo $Position; ?></td>
					<td><?php echo $Division; ?></td>
					<td><?php echo $Office; ?></td>
					<td></td>
					<td><?php echo $JobGrade; ?></td>
					<td></td>
					<td class="text-center"><?php echo $HiredDate; ?></td>
					<td></td>
					<td><?php echo $ApptStatus; ?></td>
					<td></td>
					<td><?php echo $EmailAdd; ?></td>
					<td><?php echo $ResiAddCity; ?></td>
					<td><?php echo $MobileNo; ?></td>
					<td><?php echo $BirthDate; ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			<?php 
              } ?>
			</tbody>
		</table>
	</body>
</html>