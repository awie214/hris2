<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="16" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>
               <tr class="colHEADER" align="center">
                  <th rowspan="2">ID</th>
                  <th rowspan="2">EMPLOYEE NAME</th>
                  <th rowspan="2">POSITION</th>
                  <th rowspan="2">ASSUMPTION</th>
                  <th rowspan="2">EFFECTIVE<br>DATE OF<br>PROMOTION</th>
                  <th rowspan="2">STATUS<br>OF<br>EMPLOYMENT</th>
                  <th rowspan="2">NATURE<br>OF<br>APPOINTMENT</th>
                  <th rowspan="2">SALARY</th>
                  <th colspan="3">YEAR END BONUS</th>
                  <th colspan="3">CASH</th>
                  <th rowspan="2">W/ TAX</th>
                  <th rowspan="2">TOTAL</th>
               </tr>
               <tr class="colHEADER" align="center">
                  <th>ENTITLED</th>
                  <th>RECEIVED</th>
                  <th>BALANCED</th>
                  <th>ENTITLEMENT</th>
                  <th>RECEIVED</th>
                  <th>BALANCED</th>
               </tr>

            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="16">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>