<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) { echo "DBG >> ".$whereClause; }
?>
<!DOCTYPE html>
<html>
   	<head>
      	<?php include_once $files["inc"]["pageHEAD"]; ?>
      	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   	</head>
   	<body>
      	<div class="container-fluid rptBody">
		<table>
			<thead>
				<tr>
                  <th colspan="5" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <?php spacer(30); ?>
                  </th>
               </tr> 
				<tr class="colHEADER">
					<th>Name</th>
					<th>Positon Title</th>
					<th>Status of Appointment</th>
					<th>Mode of Separation</th>
					<th>Effectivity Date of Separation</th>
				</tr>
			</thead>
			<tbody>
				<?php 
               while ($row = mysqli_fetch_assoc($rsEmployees)) {
                  $EmployeesRefId = $row["RefId"];
                     $CompanyRefId   = $row["CompanyRefId"];
                     $BranchRefId    = $row["BranchRefId"];
                     $where  = "WHERE CompanyRefId = $CompanyRefId";
                     $where .= " AND BranchRefId = $BranchRefId"; 
                     $where .= " AND EmployeesRefId = $EmployeesRefId";
                     $empinfo_row = FindFirst("empinformation",$where,"*");
                        if ($empinfo_row) {
                           $Position  = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                           $ApptStatus = getRecord("ApptStatus",$empinfo_row["ApptStatusRefId"],"Name");
                        } else
                           $Position = "";
                           $ApptStatus = "";
               ?>
					<tr>
						<td><?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]; ?></td>
						<td><?php echo $Position; ?></td>
						<td><?php echo $ApptStatus; ?></td>
						<td></td>
						<td></td>
					</tr>
				<?php
					}
				?>	
			</tbody>
		</table>
	</body>
</html>