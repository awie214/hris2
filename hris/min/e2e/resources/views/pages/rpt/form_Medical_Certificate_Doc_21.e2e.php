<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            #Content {
               font-size: 12pt;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  $BirthDate  = $row_emp["BirthDate"];
                  $Today      = date("Y-m-d",time());
                  $Age        = intval(dateDifference($BirthDate,$Today) / 365);
                  /*if ($emp_info) {*/
                     $date_hired = $emp_info["HiredDate"];
                     if ($date_hired != "") {
                        $date_hired = date("m/d/Y",strtotime($date_hired));
                     } else {
                        $date_hired = "(NO HIRED DATE)";
                     }
                     $position   = rptDefaultValue($emp_info["PositionRefId"],"position");
         ?>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <br><br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p>To Whom It May Concern:</p>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 5%;">
                        This is to certify that I have seen and examined <b><?php echo strtoupper($FullName); ?></b>, <b><?php echo $Age; ?></b> years old, <b><?php echo $position; ?></b> for medical  clearance  prior  to <b>(purpose)</b>.  Presently, (He/She) is physically and mentally fit to <b>(purpose)</b>.
                     </p>
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12 text-right">
                     Name of Physician
                     <br>
                     ____________________
                     <br>
                     <b>License  No. ______</b>
                     <br>
                     Company  Physician
                  </div>
               </div>
            </div>
         </div>
         <?php
                  //}
               }
            }
         ?>
      </div>
   </body>
</html>