<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   /*
   if ($from == "") {
      $from     = date("Y-m-d",time());
      $from_arr = explode("-", $from);
      $from     = $from_arr[0]."-".$from_arr[1]."-01";
   }
   if ($to == "") {
      $to     = date("Y-m-d",time());
      $to_arr = explode("-", $to);
      
      $to     = $to_arr[0]."-".$to_arr[1]."-".cal_days_in_month(CAL_GREGORIAN,$to_arr[1],$to_arr[0]);
   }
   */
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) { 
               $EmployeesRefId = $row["RefId"];
               $CompanyRefId   = $row["CompanyRefId"];
               $BranchRefId    = $row["BranchRefId"];
               $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
               if ($emp_row) {
                  $appt    = $emp_row["ApptStatusRefId"];
                  $appt    = getRecord("apptstatus",$appt,"Name");
                  $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                  $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                  $worksched = $emp_row["WorkScheduleRefId"];
               } else {
                  $appt    = "";
                  $div     = "";
                  $hired   = "";
                  $worksched = "";
               }
               if ($worksched != "") {
                  rptHeader(getRptName(getvalue("drpReportKind")));
      ?>               
               <div class="row" style="padding:10px;">
                  <div class="col-sm-4">
                     <?php 
                        echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                     ?>
                  </div>
               </div>
               <table border="1" width="100%">
                  <thead>
                     <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Used Leave</th>
                        <th>FL</th>
                        <th>Earnings</th>
                        <th>Balance</th>
                     </tr>
                  </thead>   
                  <tbody>
                     <tr>
                        <th>BEGINNING BALANCE</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                           <?php
                              $table = "employeescreditbalance";
                              $whereClause = "WHERE EmployeesRefId = ".$row["RefId"]." AND NameCredits = 'VL'";
                              $vl_rs = FindLast($table,$whereClause,"*");
                              if ($vl_rs) {
                                 $vl = $vl_rs["BeginningBalance"];
                              } else {
                                 $vl = 0;
                              }
                              ${"VLBal_".$EmployeesRefId} = $vl;
                              echo $vl;
                           ?>
                        </th>
                        <th></th>
                     </tr>
                     <?php
                        ${"arr_leave_".$EmployeesRefId} = [
                           "01"=>"",
                           "02"=>"",
                           "03"=>"",
                           "04"=>"",
                           "05"=>"",
                           "06"=>"",
                           "07"=>"",
                           "08"=>"",
                           "09"=>"",
                           "10"=>"",
                           "11"=>"",
                           "12"=>"",
                        ];
                        $where = "where EmployeesRefId = ".$EmployeesRefId." AND Status = 'Approved'";
                        $where .= " AND ApplicationDateFrom > '".$year."-01-01"."' ORDER BY ApplicationDateFrom";
                        $rsLeave = SelectEach("employeesleave",$where);
                        if ($rsLeave) {
                           while ($row = mysqli_fetch_assoc($rsLeave)) {
                              $dfrom   = date("d",strtotime($row["ApplicationDateFrom"]));
                              $dto     = date("d",strtotime($row["ApplicationDateTo"]));
                              $type    = getRecord("leaves",$row["LeavesRefId"],"Code");
                              $leave_month   = date("m",strtotime($row["ApplicationDateFrom"]));
                              $date    = $row["ApplicationDateFrom"];
                              $arr     = [$type,$row["ApplicationDateFrom"],$row["ApplicationDateTo"]];
                              ${"arr_leave_".$EmployeesRefId}[$leave_month] = [$date];
                              ${"arr_leave_".$EmployeesRefId}[$leave_month][$date] = $arr;
                              /*var_dump(${"arr_leave_".$EmployeesRefId}[$leave_month]);
                              return false;*/
                           }
                        }
                        $arr_month =[
                          "January",
                          "February",
                          "March",
                          "April",
                          "May",
                          "June",
                          "July",
                          "August",
                          "September",
                          "October",
                          "November",
                          "December"
                        ];
                        for ($a=1; $a <=12 ; $a++) { 
                           if ($a <= 9) $a = "0".$a;
                           ${"VL_".$a} = 0;
                           ${"SL_".$a} = 0;
                        }
                        for ($i=0; $i <= date("m",time()) - 2 ; $i++) { 
                           $idx = $i+1;
                           if ($idx <= 9) $idx = "0".$idx;
                           $month      = $idx;
                           $lastday    = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                           $final_date = $year."-".$month."-".$lastday;
                           $first_date = $year."-".$month."-01";
                           /*$arr_empDTR = getDTRSummary($first_date,
                                                $final_date,
                                                $EmployeesRefId,
                                                $CompanyRefId,
                                                $BranchRefId);*/
                           $arr_empDTR = getDTRSummary($EmployeesRefId,intval($month),$year);
                           echo '
                              <tr>
                                 <td class="text-center">'.$arr_month[$i].'</td>';
                           echo '<td>';
                                 
                           echo '</td>';      
                           echo '<td class="text-center">';
                              if (${"arr_leave_".$EmployeesRefId}[$idx] != "") {
                                 foreach (${"arr_leave_".$EmployeesRefId}[$idx] as $key => $value) {
                                    $type = $value[0];
                                    $from = intval(date("d",strtotime($value[1])));
                                    $to   = intval(date("d",strtotime($value[2])));
                                    $val1 = date("F d,Y",strtotime($value[1]));
                                    $val2 = date("F d,Y",strtotime($value[2]));
                                    if (strtotime($value[1]) > 0) {
                                       if ($from == $to) {
                                          echo $val1." - ".$val2." (".$type.") ";
                                          switch ($type) {
                                             case 'VL':
                                                ${"VL_".$idx}++;
                                                break;
                                             case 'SL':
                                                ${"SL_".$idx}++;
                                                break;
                                          }
                                       } else {
                                          $count = "";
                                          for ($x=$from; $x <= $to; $x++) { 
                                             $count = $count.$x.",";
                                             switch ($type) {
                                                case 'VL':
                                                   ${"VL_".$idx}++;
                                                   break;
                                                case 'SL':
                                                   ${"SL_".$idx}++;
                                                   break;
                                             }
                                          }
                                          echo $val1." - ".$val2." (".$type.") ";
                                       }
                                    }
                                 }
                              }
                           echo '</td>';      
                           echo '<td class="text-center">';
                              echo $arr_empDTR["FLCount"];
                           echo '</td>';      
                           echo '<td class=  "text-center">';
                              echo $arr_empDTR["UAL"];
                           echo '</td>';      
                           echo '<td class="text-center">';
                              $UALVL = $arr_empDTR["UAL"] + ${"VL_".$idx};
                              $newVLBal = ${"VLBal_".$EmployeesRefId} + $arr_empDTR["VLEarned"] - $UALVL;
                              echo $newVLBal;
                              ${"VLBal_".$EmployeesRefId} = $newVLBal;   
                           echo '</td>';        
                           echo '
                              </tr>
                           ';
                        }
                     ?>
                  </tbody>   
               </table>
               <p>
                  This is a system generated report. Signature is not required.
               </p>
         <?php 
               }
            }
         }
         ?>
      </div>
   </body>
</html>