<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $count++;
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  if ($emp_info) {
                     $Salary = $emp_info["SalaryAmount"];
                     if ($Salary != "") {
                        $Salary = number_format($Salary,2);
                     } else {
                        $Salary = "";
                     }
                  } else {
                     $Salary = "";
                  }
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="6">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     NAME: <?php echo $FullName; ?> 
                  </td>
                  <td colspan="3">
                     Salary: <?php echo $Salary; ?> 
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">DATE</th>
                  <th>OVERTIME</th>
                  <th colspan="4">MINUTES OVERTIME</th>
               </tr>
               <tr class="colHEADER">
                  <th>IN</th>
                  <th>OUT</th>
                  <th>&nbsp;</th>
                  <th>WEEKDAYS</th>
                  <th>WEEKEND</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  for ($i=1; $i <= 10; $i++) { 
                     echo '
                        <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                     ';
                  }
               ?>
               <tr>
                  <td colspan="3" class="text-center">Total OT (Minutes)</td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td colspan="3" class="text-center">Total OT (Hours)</td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td colspan="3" class="text-center">OT Payment</td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td colspan="3" class="text-center">Total OT Payment</td>
                  <td></td>
                  <td colspan="2"></td>
               </tr>
            </tbody>
         </table>
         <br>
         <br>
         <div class="row margin-top">
            <div class="col-xs-6">
               Prepared by:
               <br>
               <b>Laila R. Porlucas</b>
               <br>
               Admin. Officer IV
            </div>
            <div class="col-xs-6">
               Noted by:
               <br>
               <b>Jelly N. Ortiz, DPA</b>
               <br>
               Supvg. Admin. Officer
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>