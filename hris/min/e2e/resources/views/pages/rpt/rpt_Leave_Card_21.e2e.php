<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  if ($emp_info) {
                     $Division = rptDefaultValue($emp_info["DivisionRefId"],"division");
                     $HiredDate = $emp_info["HiredDate"];
                  } else {
                     $Division = "";
                     $HiredDate = "";
                  }
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr>
                        <td colspan="17">
                           <?php
                              rptHeader(getvalue("RptName"));
                           ?>   
                        </td>
                     </tr>
                     <tr>
                        <td colspan="17">&nbsp;</td>
                     </tr>
                     <tr>
                        <td colspan="9">
                           NAME: <?php echo strtoupper($FullName); ?>   
                        </td>
                        <td colspan="8">
                           EMPLOYEE ID NO: <?php echo $row_emp["AgencyId"]; ?>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="7">
                           DIVISION: <?php echo $Division; ?>
                        </td>
                        <td colspan="5">
                           1ST DAY OF SERVICE: <?php echo $HiredDate; ?>
                        </td>
                        <td colspan="5">
                           LAST DAY OF SERVICE: 
                        </td>
                     </tr>
                     <tr class="colHEADER">
                        <th rowspan="2" style="width: 3%;">MO<br>S.</th>
                        <th rowspan="2" style="width: 15%;">DESCRIPTION</th>
                        <th rowspan="2" style="width: 7%;">DATE/S COVERED</th>
                        <th rowspan="2" style="width: 3%;">DAYS/<br>MINS.</th>
                        <th rowspan="2" style="width: 3%;">NO. OF<br>TARDY<br>UT</th>
                        <th colspan="4">VACATION LEAVE CREDITS</th>
                        <th colspan="4">SICK LEAVE CREDITS</th>
                        <th colspan="3">OTHER LEAVE DETAILS</th>
                        <th rowspan="2" style="width: 15%;">REMARKS</th>
                     </tr>
                     <tr class="colHEADER">
                        <th style="width: 4%;">ABS/T/<br>UTwith<br>PAY</th>
                        <th style="width: 4%;">EARNED</th>
                        <th style="width: 4%;">BAL.</th>
                        <th style="width: 4%;">w/o PAY</th>
                        <th style="width: 4%;">ABS/T/<br>UTwith<br>PAY</th>
                        <th style="width: 4%;">EARNED</th>
                        <th style="width: 4%;">BAL.</th>
                        <th style="width: 4%;">w/o PAY</th>
                        <th style="width: 15%;">TYPES OF LEAVES</th>
                        <th style="width: 7%;">DATE/S COVERED</th>
                        <th style="width: 4%;">NO. OF<br>DAYS</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        include 'inc/inc_leave_card_month_1-6.php';
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr>
                        <td colspan="17">
                           <?php
                              rptHeader(getvalue("RptName"));
                           ?>   
                        </td>
                     </tr>
                     <tr>
                        <td colspan="17">&nbsp;</td>
                     </tr>
                     <tr>
                        <td colspan="9">
                           NAME: <?php echo strtoupper($FullName); ?>   
                        </td>
                        <td colspan="8">
                           EMPLOYEE ID NO: <?php echo $row_emp["AgencyId"]; ?>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="7">
                           DIVISION: <?php echo $Division; ?>
                        </td>
                        <td colspan="5">
                           1ST DAY OF SERVICE: <?php echo $HiredDate; ?>
                        </td>
                        <td colspan="5">
                           LAST DAY OF SERVICE: 
                        </td>
                     </tr>
                     <tr class="colHEADER">
                        <th rowspan="2" style="width: 3%;">MO<br>S.</th>
                        <th rowspan="2" style="width: 15%;">DESCRIPTION</th>
                        <th rowspan="2" style="width: 7%;">DATE/S COVERED</th>
                        <th rowspan="2" style="width: 3%;">DAYS/<br>MINS.</th>
                        <th rowspan="2" style="width: 3%;">NO. OF<br>TARDY<br>UT</th>
                        <th colspan="4">VACATION LEAVE CREDITS</th>
                        <th colspan="4">SICK LEAVE CREDITS</th>
                        <th colspan="3">OTHER LEAVE DETAILS</th>
                        <th rowspan="2" style="width: 15%;">REMARKS</th>
                     </tr>
                     <tr class="colHEADER">
                        <th style="width: 4%;">ABS/T/<br>UTwith<br>PAY</th>
                        <th style="width: 4%;">EARNED</th>
                        <th style="width: 4%;">BAL.</th>
                        <th style="width: 4%;">w/o PAY</th>
                        <th style="width: 4%;">ABS/T/<br>UTwith<br>PAY</th>
                        <th style="width: 4%;">EARNED</th>
                        <th style="width: 4%;">BAL.</th>
                        <th style="width: 4%;">w/o PAY</th>
                        <th style="width: 15%;">TYPES OF LEAVES</th>
                        <th style="width: 7%;">DATE/S COVERED</th>
                        <th style="width: 4%;">NO. OF<br>DAYS</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        include 'inc/inc_leave_card_month_7-12.php';
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>