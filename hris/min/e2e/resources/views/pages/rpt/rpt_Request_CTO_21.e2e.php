<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $count++;
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  if ($emp_info) {
                     $Position = rptDefaultValue($emp_info["PositionRefId"],"position");
                     $Division = rptDefaultValue($emp_info["DivisionRefId"],"division");
                  } else {
                     $Position = "";
                     $Division = "";
                  }
                  /*$emp_cto = SelectEach("employeescto","WHERE EmployeesRefId = ".$row_emp["RefId"]." AND Status = 'Approved'");
                  if ($emp_cto) {
                     while ($cto_row = mysqli_fetch_assoc($emp_cto)) {*/

                  
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6"></div>
                  <div class="col-xs-6 text-right">
                     DATE OF FILING: <?php //echo date("m/d/Y",strtotime($cto_row["FiledDate"])) ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     NAME: <?php echo rptDefaultValue($FullName); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     POSITION: <?php echo rptDefaultValue($Position); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     DIVISION: <?php echo rptDefaultValue($Division); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     SCHEDULE OF CTO: 
                  </div>
                  <div class="col-xs-6">
                     NUMBER OF HOURS: <?php //echo $cto_row["Hours"]; ?>
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4">
                     Requested by:
                  </div>
                  <div class="col-xs-4">
                     Recommending Approval
                  </div>
                  <div class="col-xs-4">
                     Approved By: 
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4 text-center">
                     ___________________________
                     <br>
                     Print Name and Signature
                  </div>
                  <div class="col-xs-4 text-center">
                     ___________________________
                     <br>
                     Division Head
                  </div>
                  <div class="col-xs-4 text-center">
                     ___________________________
                     <br>
                     Executive Director /
                     <br>
                     Authorized Official
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Attachment: <b><i>Certificate of Overtime Credits</i></b>
                  </div>
               </div>
            </div>
         </div>
         <?php
                   /*  }
                  }*/
               }
            }
         ?>
      </div>
   </body>
</html>