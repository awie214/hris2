<?php
   $emp_refid = "";
   if (getvalue("hucode") == "COMPEMP") {
      $emp_refid = getvalue("hEmpRefId");
   }
   include_once 'constant.e2e.php';
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="javascript">
         $(document).ready(function(){
            $(".img-thumbnail").click(function(){
               $("#imgHolder").attr("src",$(this).attr("src"));
               $("#imgFilename").html($(this).attr("fname"));
               $("#imgModal").modal();
            });
            $("#btnATTACH").click(function() {
               if ($("[name='txtRefId']").val() == "") {
                  alert("Please Enter Employees RefId!!!");
                  return;
               } else {
                  emprefid = $("[name='txtRefId']").val();
                  if (emprefid <= 0) {
                     alert("Please Enter Employees RefId!!!");
                     return;
                  }
                  gotoscrn('DocAttachUpload','&paramTitle=Attach Document&txtRefId='+emprefid);
               }
            });

         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top">
               <?php
                  $isUser = false;
                  if ($GLOBALS['UserCode'] != "COMPEMP") {
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $emp_refid = EmployeesSearch($attr);
                     spacer(10);
                     echo '
                        <button type="button" class="btn-cls4-sea" id="btnATTACH" name="btnATTACH">
                           <i class="fa fa-paperclip" aria-hidden="true"></i>
                           &nbsp;&nbsp;ATTACHMENT
                        </button>
                     ';
                  } else {
                     spacer(10);
                     echo
                     '<button type="button" class="btn-cls4-sea" id="btnATTACH" name="btnATTACH">
                     <i class="fa fa-paperclip" aria-hidden="true"></i>
                     &nbsp;&nbsp;ATTACHMENT
                     </button>';
                     $isUser = true;
                     $rsEmployees = FFirstRefId('employees',getvalue("hEmpRefId"),'*');
                     $emp_refid = $rsEmployees["RefId"];
                     $emp_LastName = $rsEmployees["LastName"];
                     $emp_FirstName = $rsEmployees["FirstName"];
                     $emp_MiddleName = $rsEmployees["MiddleName"];
                     spacer(10);
               ?>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <span class="parentBadge-silver">
                           EMP. REF.ID :<span class="badge childBadge-silver"><?php echo $emp_refid; ?></span>
                        </span>
                        <span class="parentBadge-silver">
                           LAST NAME :<span class="badge childBadge-silver"><?php echo $emp_LastName; ?></span>
                        </span>
                        <span class="parentBadge-silver">
                           FIRST NAME :<span class="badge childBadge-silver"><?php echo $emp_FirstName; ?></span>
                        </span>
                        <span class="parentBadge-silver">
                           MIDDLE NAME :<span class="badge childBadge-silver"><?php echo $emp_MiddleName; ?></span>
                        </span>
                     </div>
                  </div>
               <?php
                  spacer(1);
                  }
               ?>
               <div class="row margin-top10">
                  <?php
                  if ((isset($_POST["submit"]) && $_POST["submit"] == "submit") || $isUser) {
                        $dir = path."images/".getvalue("hCompanyID")."/EmpDocument";
                        $fileName = "";
                        if (is_dir($dir)){
                           if ($dh = opendir($dir)){
                              $hadDoc = false;
                              while (($file = readdir($dh)) !== false)
                              {
                                 if ($file != "." &&
                                     $file != "..") {
                                    if (stripos($file,".png") > 0 ||
                                        stripos($file,".jpg") > 0 ||
                                        stripos($file,".gif") > 0 ||
                                        stripos($file,".jpeg") > 0 ||
                                        stripos($file,".pdf") > 0 )

                                    {
                                       $file_Arr = explode("_",$file);
                                       if (count($file_Arr) >= 2) {
                                          $doctype = $file_Arr[0];
                                          $emprefid = explode(".", $file_Arr[1])[0];
                                          if ($emprefid == $emp_refid) {
                                             $hadDoc = true;
                                             $fileName .= $file."|";
                                          }
                                       }
                                    }
                                 }
                              }
                              closedir($dh);
                           }
                        }
                  ?>
                  <div class="col-xs-1"></div>
                  <div class="col-xs-10">

                     <?php if ($fileName != "") { ?>
                     <!-- <div id="myDocs" class="carousel slide" data-ride="carousel"> -->
                     <div>
                        <ol class="carousel-indicators">
                           <?php
                              $file_Array = explode("|",$fileName);
                              for ($h=0;$h<count($file_Array) - 1;$h++) {
                                 if ($h == 0) {
                                    echo '<li data-target="#myDocs" data-slide-to="0" class="active"></li>';
                                 } else {
                                    echo '<li data-target="#myDocs" data-slide-to="'.$h.'"></li>';
                                 }
                              }
                           ?>
                        </ol>

                        <div>
                        <?php
                           for ($h=0;$h<count($file_Array) - 1;$h++) {
                              /*if ($h == 0) {
                                 $class = "item active";
                              } else {
                                 $class = "item";
                              }*/

                              if (stripos($file_Array[$h],".pdf") > 0) {
                                 echo
                                 '<div class="container">
                                   <h3>'.$file_Array[$h].'
                                    <button type="button" class="btn-cls4-sea">
                                       <a href="'.$dir.'/'.$file_Array[$h].'" style="color:white !important; text-decoration:none !important;" download>
                                          <i class="fa fa-arrow-down"></i> DOWNLOAD
                                       </a>
                                    </button>
                                   </h3>
                                   <embed src="'.$dir.'/'.$file_Array[$h].'" style="height:500px;"/>
                                 </div>'."\n";
                              } else {
                                 echo
                                 '<div class="container">
                                   <h3>'.$file_Array[$h].'</h3>
                                   <img src="'.$dir.'/'.$file_Array[$h].'" fname="'.$file_Array[$h].'" class="img-thumbnail">
                                 </div>'."\n";
                              }

                              /*if (stripos($file_Array[$h],".pdf") > 0) {
                                 echo
                                 '<div class="'.$class.'" style="height:400px;">
                                    <embed src="'.$dir.'/'.$file_Array[$h].'" style="margin:auto;width:100%;"/>
                                    <div class="carousel-caption">
                                       <h3>'.$file_Array[$h].'</h3>
                                    </div>
                                 </div>';
                              } else {
                                 echo
                                 '<div class="'.$class.'" style="height:400px;">
                                    <img src="'.$dir.'/'.$file_Array[$h].'" style="margin:auto;height:100%;">
                                    <div class="carousel-caption">
                                       <h3>'.$file_Array[$h].'</h3>
                                    </div>
                                 </div>';
                              }*/
                           }
                        ?>
                        </div>

                        <!--
                        <a class="left carousel-control" href="#myDocs" data-slide="prev">
                           <span class="glyphicon glyphicon-chevron-left"></span>
                           <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myDocs" data-slide="next">
                           <span class="glyphicon glyphicon-chevron-right"></span>
                           <span class="sr-only">Next</span>
                        </a>
                        -->

                     </div>
                     <?php } else {
                        echo '<h1>NO DOCS FOUND</h1>';
                     } ?>
                  </div>
                  <div class="col-xs-1"></div>
                  <?php } ?>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               fillEmpLkUp (
                  getvalue("txtRefId"),
                  getvalue("txtLName"),
                  getvalue("txtFName"),
                  getvalue("txtMidName")
               )
            ?>
         </div>
         <!-- Modal -->
         <div id="imgModal" class="modal fade" role="dialog">
           <div class="modal-dialog modal-lg">

             <!-- Modal content-->
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h3 id="imgFilename"></h3>
               </div>
               <div class="modal-body txt-center">
                  <img src="" width="90%" id="imgHolder">
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>

           </div>
         </div>
      </form>
   </body>
</html>



