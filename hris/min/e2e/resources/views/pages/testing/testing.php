<?php
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	include pathClass.'DTRFunction.e2e.php';

	function TH($timeInMin) {
        if ($timeInMin > 0) {
           	$hr = explode(".",$timeInMin / 60)[0];
           	if ($hr <= 9) {
              	$hr = "0".$hr;
           	}
           	$mod_min = ($timeInMin % 60);
           	if ($mod_min <= 9) {
              	$mod_min = "0".$mod_min;
           	}
           	return $hr.":".$mod_min;   
        } else {
           return "&nbsp;";
        }
    }

	$rs = getDTRSummary("4","2",2018);
	foreach ($rs as $key => $value) {
		echo $key."------>".$value."<br>";
	}
	$TotalRegHrs 			= $rs["TotalRegHrs"];
	$TotalExcessHrs 		= $rs["TotalExcessHrs"];
	$TotalHours				= $rs["TotalHours"];
	$TotalLate				= $rs["TotalLate"];
	$TotalCOC				= $rs["TotalCOC"];
	$TotalOT				= $rs["TotalOT"];
	$TotalUnderTime			= $rs["TotalUnderTime"];
	$LateCount				= $rs["LateCount"];
	$UnderTimeCount			= $rs["UnderTimeCount"];
	$AbsentCount			= $rs["AbsentCount"];
	$COCCount				= $rs["COCCount"];
	$HoursEquivalent		= $rs["HoursEquivalent"];
	$VLEarned				= $rs["VLEarned"];
	$SLEarned				= $rs["SLEarned"];
	$UAL					= $rs["UAL"];

	echo getEquivalent($TotalUnderTime,"workinghrsconversion");
	echo "<br>".$AbsentCount."<br>".$UAL;
?>