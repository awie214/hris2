<?php
	include 'FnUpload.php';

	mysqli_query($conn,"TRUNCATE employeeschild");
	$EmpChild = fopen("csv/emp_child1_28.csv", "r");
	while(!feof($EmpChild)) {
		$child_row = explode(",", fgets($EmpChild));
		$EmpAgencyID = clean($child_row[0]);
		$ChildName = clean($child_row[1]);
		$ChildBday = $child_row[2];
		if (strpos($ChildBday, "/") > 1) {
			if ($ChildBday != "") {
	        	$ChildBday_arr = explode("/",$ChildBday);
	        	$day = $ChildBday_arr[0];
	        	$month = $ChildBday_arr[1];
	        	$year = $ChildBday_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$ChildBday = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$ChildBday = "";
		}
		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		if (is_numeric($emprefid)) {
			$Flds = "CompanyRefId, BranchRefId, EmployeesRefId, FullName, ";
			$Vals = "28, 1, $emprefid, '$ChildName',";
			if ($ChildBday != "") {
				$Flds .= "BirthDate, ";
				$Vals .= "'$ChildBday', ";
			}
			$save_emp_child = save("employeeschild",$Flds,$Vals);
			if (is_numeric($save_emp_child)) {
				echo $emprefid." -> ".$ChildName." Saved<br>";
			}
		}
	}
?>