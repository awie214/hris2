<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	$EmpInfo = fopen("csv/employee_info_1000.csv", "r");
	$count = 0;
	while(!feof($EmpInfo)) {
		$employee_fldnval 	= "";
		$plantilla_fldnval 	= "";
		$empinfo_fldnval 	= "";
		$obj_row 			= explode(",", fgets($EmpInfo));
		$empid 				= clean($obj_row[0]);
		$PositionRefId 		= clean($obj_row[1]);
		$SalaryGradeRefId 	= clean($obj_row[2]);
		$StepIncrementRefId = clean($obj_row[3]);
		$SalaryAmount 		= clean($obj_row[4]);
		$DepartmentRefId 	= clean($obj_row[5]);
		$DivisionRefId 		= clean($obj_row[6]);
		$PositionItemRefId 	= clean($obj_row[7]);
		$EmpStatusRefId		= clean($obj_row[8]);
		$BPNumber 			= clean($obj_row[9]);
		$PHIC 				= clean($obj_row[10]);
		$PAGIBIG 			= clean($obj_row[11]);
		$TIN 				= clean($obj_row[12]);
		$emprefid           = FindFirst("employees","WHERE AgencyId = '$empid'","RefId",$conn);
		if ($emprefid) {
			if ($PositionRefId != "") {
				$PositionRefId = saveFM("position","`Name`, ","'$PositionRefId', ",$PositionRefId);
				$plantilla_fldnval .= "`PositionRefId` = '$PositionRefId',";
				$empinfo_fldnval   .= "`PositionRefId` = '$PositionRefId',";
			}
			if ($SalaryGradeRefId != "") {
				$SalaryGradeRefId = saveFM("salarygrade","`Name`, ","'$SalaryGradeRefId', ",$SalaryGradeRefId);
				$plantilla_fldnval .= "`SalaryGradeRefId` = '$SalaryGradeRefId',";
				$empinfo_fldnval   .= "`SalaryGradeRefId` = '$SalaryGradeRefId',";
			}
			if ($StepIncrementRefId != "") {
				$StepIncrementRefId = saveFM("stepincrement","`Name`, ","'$StepIncrementRefId', ",$StepIncrementRefId);
				$plantilla_fldnval .= "`StepIncrementRefId` = '$StepIncrementRefId',";
				$empinfo_fldnval   .= "`StepIncrementRefId` = '$StepIncrementRefId',";
			}
			if ($DivisionRefId != "") {
				$DivisionRefId = saveFM("division","`Name`, ","'$DivisionRefId', ",$DivisionRefId);
				$plantilla_fldnval .= "`DivisionRefId` = '$DivisionRefId',";
				$empinfo_fldnval   .= "`DivisionRefId` = '$DivisionRefId',";
			}
			if ($EmpStatusRefId != "") {
				$EmpStatusRefId = saveFM("empstatus","`Name`, ","'$EmpStatusRefId', ",$EmpStatusRefId);
				$empinfo_fldnval   .= "`EmpStatusRefId` = '$EmpStatusRefId',";
			}
			if (intval($SalaryAmount) > 0) {
				$plantilla_fldnval .= "`SalaryAmount` = '$SalaryAmount',";
				$empinfo_fldnval   .= "`SalaryAmount` = '$SalaryAmount',";	
			}
			if ($PHIC != "") {
				$employee_fldnval .= "`PHIC` = '$PHIC', ";
			}
			if ($PAGIBIG != "") {
				$employee_fldnval .= "`PAGIBIG` = '$PAGIBIG', ";
			}
			if ($TIN != "") {
				$employee_fldnval .= "`TIN` = '$TIN', ";
			}

			if ($PositionItemRefId != "") {
				if (strtolower($PositionItemRefId) != "contractual") {
					$PositionItemRefId = saveFM("positionitem","`Name`, ","'$PositionItemRefId', ",$PositionItemRefId);
					$empinfo_fldnval   .= "`PositionItemRefId` = '$PositionItemRefId',";	
				}
			}

			if ($plantilla_fldnval != "") {
				if (strtolower($PositionItemRefId) != "contractual") {
					$update_plantilla = update("positionitem",$plantilla_fldnval,$PositionItemRefId);
					if ($update_plantilla != "") echo "Error in Saving Plantilla.<br>";
				}
			}

			$empinfo_refid = FindFirst("empinformation","WHERE EmployeesRefId ='$emprefid'","RefId",$conn);
			if ($empinfo_refid) {
				if ($empinfo_fldnval != "") {
					$update_empinfo = update("empinformation",$empinfo_fldnval,$empinfo_refid);
					if ($update_empinfo != "") echo "Error in Updating Employee Info.<br>";
				}	
			}
			if ($employee_fldnval != "") {
				$update_employee = update("employees",$employee_fldnval,$emprefid);
				if ($update_employee != "") echo "Error in Updating Employee Info.<br>";
			}
		}
			
	}
?>