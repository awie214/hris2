<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	$file = fopen(textFile."holidays.txt","r");
	$date_today    = date("Y-m-d",time());
	$curr_time     = date("H:i:s",time());
	$trackingflds = "`LastUpdateBy`, `LastUpdateDate`, `LastUpdateTime`, `Data`";
	$trackingvals = "'SYSTEM', '$date_today', '$curr_time', 'A'";
	while(! feof($file)) {
	  	$obj = fgets($file);
	  	$arr = explode("_", $obj);
	  	if ($arr[0] != "") {
	  		$name = realEscape(trim($arr[0]));
		  	$date = $arr[1];
			$flds = "`Name`,`StartDate`,`EndDate`, `isLegal`, `isApplyEveryYr`, ";
			$values = "'$name', '$date', '$date', '1', 1,";
			$result = f_SaveRecord("NEWSAVE","holiday",$flds,$values);
			if (is_numeric($result)) {
				echo "$name uploaded<br>";
			} else {
				echo $result." $name";
			}

	  	}
	  	
	}

?>