<?php
    include 'conn.e2e.php';
    include "constant.e2e.php";

    $table = "schools";

    //mysqli_query($conn,"TRUNCATE TABLE $table");
	$myfile = fopen(textFile."Schools_Update.csv", "r") or die("Unable to open file!");
	// Output one line until end-of-file
	while(!feof($myfile)) {
		$a = fgets($myfile);
		$a = mysqli_real_escape_string($conn,trim(str_replace("'","", $a)));
		$a = mysqli_real_escape_string($conn,trim(str_replace('"',"", $a)));

		if ($a == "#OfferType1") {
			$OfferType = 1;
		} else if ($a == "#OfferType2") {
			$OfferType = 2;
		} else if ($a == "#OfferType3") {
			$OfferType = 3;
		} else if ($a == "#OfferType4") {
			$OfferType = 4;
		} else {
	        $a = trim(strtoupper($a));
			date_default_timezone_set("Asia/Manila");
			$t             = time();
	        $date_today    = date("Y-m-d",$t);
	        $curr_time     = date("H:i:s",$t);
	        $loc_user      = "admin";
			$flds = "";
			$trackingA_fld = "";
			$trackingA_val = "";
			$values = "";
		    //$sql = "SELECT * FROM ".strtolower($table)." where Name = '$a' AND Offer$OfferType = 1";
		    $sql = "SELECT * FROM ".strtolower($table)." WHERE Name = '$a'";
	        $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
	        if (mysqli_num_rows($result) > 0) {
	        	$r = mysqli_fetch_assoc($result);
        	    $qry_update = "UPDATE `".strtolower($table)."` SET Offer$OfferType = 1 WHERE Name = '$a'";
                if ($conn->query($qry_update) === TRUE) {
                	//echo "School $a Updated<br>";
                } else {
                	echo "ERROR UPDATE $qry_update<br>";
                }
	        } else {
	        	$flds .= "Name, Remarks, Offer$OfferType,";
				$values .= "'$a', 'Uploaded', 1, ";
				$trackingA_fld .= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
		        $trackingA_val .= "'$date_today', '$curr_time', '$loc_user', 'A'";
		        $flds   = $flds.$trackingA_fld;
		        $values = $values.$trackingA_val;
		        $sql = "INSERT INTO $table ($flds) VALUES ($values)";
		        $rs = mysqli_query($conn,$sql);
		        if ($rs) {
		        	echo "School saved!!!<br>";
		        } else {
		        	echo "Error in Saving School".$sql."<br><br>";
		        }
	        }
        }
	    
	}
    /*
	    $qry_update = "UPDATE `".strtolower($table)."` SET Offer5 = 1 WHERE Offer4 = 1";
	    if ($conn->query($qry_update) === TRUE) {
	    } else {
	    	echo "ERROR UPDATE $qry_update<br>";
	    }
    */
    echo "Upload Done...";
	mysqli_close($conn);
	fclose($myfile);
?>