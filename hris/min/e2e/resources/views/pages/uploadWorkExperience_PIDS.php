<!DOCTYPE html>
<html>
<body onload="alert('Done');">

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

            $conn->query("TRUNCATE TABLE `employeesworkexperience`;");
            $conn->query("TRUNCATE TABLE `position`;");
            $conn->query("TRUNCATE TABLE `agency`;");
            $conn->query("TRUNCATE TABLE `salarygrade`;");
            $conn->query("TRUNCATE TABLE `apptstatus`;");


            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `employee_work_experiences` WHERE employee_personal_information_sheet_id > 0 ORDER BY employee_personal_information_sheet_id";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;

                     $wPosition = strtoupper(remquote($pids['position_title']));
                     $wPositionRefId = fileManager($wPosition,
                                          "position",
                                          "`Code`,`Name`,`Remarks`,",
                                          "'','".$wPosition."','Migrated',");

                     $wAgency = strtoupper(remquote($pids["company"]));
                     $wAgencyRefId = fileManager($wAgency,
                                          "agency",
                                          "`Code`,`Name`,`Remarks`,",
                                          "'','".$wAgency."','Migrated',");

                     $wSalGrade = strtoupper($pids["salary_grade"]);
                     $wSalGradeRefId = fileManager($wSalGrade,
                                          "salarygrade",
                                          "`Code`,`Name`,`Remarks`,",
                                          "'','".$wSalGrade."','Migrated',");

                     $wApptStatus = strtoupper(remquote($pids["status_of_appointment"]));
                     $wApptStatusRefId = fileManager($wApptStatus,
                                          "apptstatus",
                                          "`Code`,`Name`,`Remarks`,",
                                          "'','".$wApptStatus."','Migrated',");

                     /*$wmonthlySal    = str_replace(",","",number_format($pids['monthly_salary'],2));
                     $wyearRate      = str_replace(",","",number_format($pids['rate_per_year'],2));*/

                     $wyearRate      = $pids['rate_per_year'];
                     $wmonthlySal    = $pids['monthly_salary'];

                     $wstep          = $pids['step'];
                     $wisGovtService = $pids['government_service'];
                     $wLWOP          = $pids['leave_of_absence_without_pay'];
                     $wWorkStartDate = $pids['start_date'];
                     $wWorkEndDate   = $pids['end_date'];
                     $wRemarks       = remquote($pids['remarks']).";Migrated";
                     $wIsServiceRecord = $pids['isservicerecord'];

                     $wReason = "";
                     $wExtDate = "";
                     $wSeparatedDate = "";
                     $wEmpStatusRefId = 0;
                     $wPayrateRefId = 0;
                     $wPayGroup = "";


                     $flds = "";
                     $values = "";
                     $flds .= "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`, `AgencyRefId`, `PositionRefId`,";
                     $flds .= "`StepIncrementRefId`, `WorkStartDate`, `WorkEndDate`, `SalaryAmount`,`isServiceRecord`,";
                     $flds .= "`ApptStatusRefId`, `isGovtService`, `Remarks`,`SalaryGradeRefId`,`YearRate`,`LWOP`,".$trackingA_fld;
                     $values .= "1000,1,$empRefId, '$wAgencyRefId', '$wPositionRefId',";
                     $values .= "'$wstep', '$wWorkStartDate', '$wWorkEndDate', '$wmonthlySal','$wIsServiceRecord',";
                     $values .= "'$wApptStatusRefId', '$wisGovtService', '$wRemarks','$wSalGradeRefId','$wyearRate','$wLWOP',".$trackingA_val;

                     $sql = "INSERT INTO `employeesworkexperience` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated Work Experience -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration Work Experience -->$empRefId<br>";
                     }
                  }
               }
            }

            mysqli_close($conn);


?>

</body>
</html>