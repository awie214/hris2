<?php
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $service_record_arr = array();
   $EmployeesRefId = getvalue("hEmpRefId");
   $whereClause = "WHERE RefId = ".$EmployeesRefId." ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   $workexp = SelectEach("employeesworkexperience","WHERE EmployeesRefId = '$EmployeesRefId' AND isGovtService = 1");
   if ($workexp) {
      while ($workexp_row = mysqli_fetch_assoc($workexp)){
         $From = $workexp_row["WorkStartDate"];
         $To   = $workexp_row["WorkEndDate"];
         $Designation = getRecord("position",$workexp_row["PositionRefId"],"Name");
         $ApptStatus = getRecord("empstatus",$workexp_row["EmpStatusRefId"],"Name");
         $Agency = getRecord("agency",$workexp_row["AgencyRefId"],"Name");
         $Salary = $workexp_row["SalaryAmount"];
         if ($From != "") $From = date("m/d/Y",strtotime($From));
         if ($To != "") {
            $To = date("m/d/Y",strtotime($To));
         } else {
            $To = "Present";
         }
         if (intval($Salary) > 0) $Salary = number_format(($Salary * 12),2);
         if ($workexp_row["Present"] == 1) $To = "Present";

         $service_record_arr[$workexp_row["WorkEndDate"]] = [
            "From"=>$From,
            "To"=>$To,
            "Designation"=>$Designation,
            "ApptStatus"=>$ApptStatus,
            "Salary"=>$Salary,
            "Remarks"=>"",
            "Agency"=>$Agency,
            "LWOP"=>"None"
         ];
      }
   }
   $empmovement = SelectEach("employeesmovement","WHERE EmployeesRefId = '$EmployeesRefId' ORDER BY ExpiryDate DESC");
   if ($empmovement) {
      while ($empmovement_row = mysqli_fetch_assoc($empmovement)) {
         $from = $empmovement_row["EffectivityDate"];
         $to = $empmovement_row["ExpiryDate"];
         $designation = getRecord("position",$empmovement_row["PositionRefId"],"Name");
         $apptstatus = getRecord("empstatus",$empmovement_row["EmpStatusRefId"],"Name");
         $Agency = getRecord("agency",$empmovement_row["AgencyRefId"],"Name");
         $salary = $empmovement_row["SalaryAmount"];
         if ($from != "") $from = date("m/d/Y",strtotime($from));
         if ($to != "") $to = date("m/d/Y",strtotime($to));
         if (intval($salary) > 0) $salary = number_format(($salary * 12),2);
         $remarks = $empmovement_row["Remarks"];
         if ($remarks == "") {
            $remarks = $empmovement_row["Cause"];
         }
         $LWOP = $empmovement_row["LWOP"];
         if (intval($LWOP) == 0) $LWOP = "None";
         $Office = getRecord("office",$empmovement_row["OfficeRefId"],"Name");
         $Division = getRecord("Division",$empmovement_row["DivisionRefId"],"Name");
         $Agency = $Office." / ".$Division;
         $service_record_arr[$empmovement_row["ExpiryDate"]] = [
            "From"=>$from,
            "To"=>$to,
            "Designation"=>$designation,
            "ApptStatus"=>$apptstatus,
            "Salary"=>$salary,
            "LWOP"=>$LWOP

         ];
      }
   }
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("SERVICE RECORD"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-xs-10" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <?php rptHeader("Employees Service Record"); ?>
                        <?php
                           /*foreach ($service_record_arr as $key => $value) {
                              echo $key."<br>";
                           }*/
                           $count = 0;
                           if ($rsEmployees) {
                              while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                        ?>
                        <div style="page-break-after: always;">
                           <table>
                              <thead>
                                 <tr>
                                    <td colspan="8">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">
                                       <div class="row">
                                          <div class="col-xs-1"><label>NAME:</label></div>
                                          <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                                             <?php echo rptDefaultValue($row_emp["LastName"]); ?>
                                          </div>
                                          <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                                             <?php echo rptDefaultValue($row_emp["FirstName"]); ?>
                                          </div>
                                          <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                                             <?php echo rptDefaultValue($row_emp["MiddleName"]); ?>
                                          </div>
                                          <div class="col-xs-5">
                                             &nbsp;(If married woman, give also full maiden name)
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">
                                       <div class="row">
                                          <div class="col-xs-1">&nbsp;</div>
                                          <div class="col-xs-2 text-center">
                                             (Surname)
                                          </div>
                                          <div class="col-xs-2 text-center">
                                             (Given Name)
                                          </div>
                                          <div class="col-xs-2 text-center">
                                             (Middle Name)
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">
                                       <div class="row">
                                          <div class="col-xs-1"><label>BIRTH:</label></div>
                                          <div class="col-xs-2 text-center" style="border-bottom: 1px solid black;">
                                             <?php echo rptDefaultValue(date("m/d/Y",strtotime($row_emp["BirthDate"]))); ?>
                                          </div>
                                          <div class="col-xs-4 text-center" style="border-bottom: 1px solid black;">
                                             <?php echo rptDefaultValue($row_emp["BirthPlace"]); ?>
                                          </div>
                                          <div class="col-xs-5">
                                             Date herein should be checked from birth or baptismal certificate or some other reliable documents.
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">
                                       <div class="row">
                                          <div class="col-xs-1">&nbsp;</div>
                                          <div class="col-xs-2 text-center">
                                             (Date)
                                          </div>
                                          <div class="col-xs-4 text-center">
                                             (Place Of Birth)
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">
                                       <p>
                                          This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities otherwise indicated.
                                       </p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="8">&nbsp;</td>
                                 </tr>
                                 <tr class="colHEADER">
                                    <th colspan="2">SERVICES<br>(Inclusive Dates)</th>
                                    <th colspan="3">RECORDS OF APPOINTMENT</th>
                                    <th rowspan="2" style="width: 15%;">Station/Place<br>of Assignment</th>
                                    <th rowspan="2" style="width: 10%;">Leave of<br>Absence<br>Without Pay</th>
                                    <th rowspan="2" style="width: 15%;">Remarks<br>Separation<br>(Reference)</th>
                                 </tr>
                                 <tr class="colHEADER">
                                    <th style="width: 10%;">FROM</th>
                                    <th style="width: 10%;">TO</th>
                                    <th style="width: 20%;">DESIGNATIONS</th>
                                    <th style="width: 10%;">STATUS</th>
                                    <th style="width: 10%;">SALARY</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                 foreach ($service_record_arr as $key => $value) {

                                 ?>
                                    <tr>
                                       <td class="text-center">
                                          <?php 
                                             echo $value["From"];
                                          ?>
                                       </td>
                                       <td class="text-center">
                                          <?php 
                                             echo $value["To"];
                                          ?>
                                       </td>
                                       <td>
                                          <?php  echo $value["Designation"]; ?>
                                       </td>
                                       <td class="text-center">
                                          <?php  echo $value["ApptStatus"]; ?>
                                       </td>
                                       <td class="text-right">
                                          <?php
                                             echo $value["Salary"]; 
                                          ?>
                                       </td>
                                       <td class="text-center">
                                          <?php  echo $value["Agency"]; ?>
                                       </td>
                                       <td class="text-center">
                                          <?php
                                             echo $value["LWOP"];
                                          ?>
                                       </td>
                                       <td>
                                          <?php  $value["Remarks"]; ?>
                                       </td>
                                    </tr>
                                 <?php     
                                 }
                                 ?>
                                 <tr>
                                    <td class="text-center" colspan="8">--NOTHING FOLLOWS--</td>
                                 </tr>
                                 <?php

                                 /*
                                    $emprefid = $row_emp["RefId"];
                                    $emp_movement = SelectEach("employeesmovement","WHERE EmployeesRefId = $emprefid");
                                    if ($emp_movement) {
                                       while ($emp_movement_row = mysqli_fetch_assoc($emp_movement)) {
                                 ?>
                                    <tr>
                                       <td class="text-center">
                                          <?php 
                                             if ($emp_movement_row["EffectivityDate"] != "") {
                                                echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["EffectivityDate"])));   
                                             }
                                          ?>
                                       </td>
                                       <td class="text-center">
                                          <?php 
                                             if ($emp_movement_row["ExpiryDate"] != "") {
                                                echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["ExpiryDate"]))); 
                                             }
                                          ?>
                                       </td>
                                       <td>
                                          <?php echo rptDefaultValue($emp_movement_row["DesignationRefId"],"designation"); ?>
                                       </td>
                                       <td class="text-center">
                                          <?php echo rptDefaultValue($emp_movement_row["EmpStatusRefId"],"empstatus"); ?>
                                       </td>
                                       <td class="text-right">
                                          <?php
                                             $SalaryAmount = intval($emp_movement_row["SalaryAmount"]);
                                             if ($SalaryAmount != "") {
                                                echo "Php ".number_format($SalaryAmount,2);
                                             } else {
                                                echo "Php ".number_format(0,2);
                                             }
                                          ?>
                                       </td>
                                       <td class="text-center">
                                          <?php echo rptDefaultValue($emp_movement_row["DivisionRefId"],"division"); ?>
                                       </td>
                                       <td class="text-center">
                                          <?php
                                             $LWOP = intval($emp_movement_row["LWOP"]);
                                             if ($LWOP > 0) {
                                                echo $LWOP;   
                                             }
                                          ?>
                                       </td>
                                       <td>
                                          <?php echo rptDefaultValue($emp_movement_row["Cause"]); ?>
                                       </td>
                                    </tr>

                                 <?php
                                       }
                                       echo '
                                          <tr>
                                             <td class="text-center" colspan="8">--NOTHING FOLLOWS--</td>
                                          </tr>
                                       ';
                                    } else {
                                       echo '
                                          <tr>
                                             <td class="text-center" colspan="8">--NO SERVICE RECORD--</td>
                                          </tr>
                                       ';
                                    }
                                 */
                                 ?>
                              </tbody>
                           </table>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 Issued in compliance with Executive Order No. 54 dated August 10, 1954, and in accordance with Circular, No. 58, dated August 10,1954 of the System.
                              </div>
                           </div>
                           <br><br>
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 DATE: <b><?php echo date("m/d/Y",time()); ?></b>
                              </div>
                              <div class="col-xs-6">
                                 CERTIFIED CORRECT: ______________________________________
                              </div>
                           </div>
                        </div>
                        <?php
                              }
                           }
                        ?>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



